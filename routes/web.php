<?php

use App\Http\Controllers\DashController;
use App\Http\Controllers\KardexController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;


Route::get('/', [DashController::class,'index'])->middleware('auth')->name('dashboard');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', [DashController::class,'index'])->name('dashboard');
    Route::get('/products', [ProductController::class,'index'])->name('products');
    Route::get('/kardexs/{id}', [KardexController::class,'index'])->name('kardexs');
    Route::get('/order/{id}', [DashController::class,'orderAdmin'])->name('order');
    Route::get('/messages', [DashController::class,'message'])->name('messages');
    Route::get('/users', [DashController::class,'user'])->name('users');
    Route::get('/componentes', [DashController::class,'components'])->name('componentes');
    Route::get('/graficas', [DashController::class,'grafics'])->name('graficas');
    Route::get('/configservicie', [DashController::class,'servicios'])->name('configservicie');    
    
});

