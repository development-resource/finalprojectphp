<?php

namespace Database\Factories;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsernotificationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "status"=>rand(0,1),
            "user_id"=> User::all()->random()->id,
            "notification_id"=> Notification::all()->random()->id,
        ];
    }
}
