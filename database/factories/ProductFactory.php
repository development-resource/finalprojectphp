<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;



class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(3);
        return [
            "sku"=> rand(10,99).$this->faker->unique()->numerify('########').rand(10,99),
            "code"=> rand(10,99).$this->faker->unique()->numerify('####').rand(10,99),
            "name"=> $name,
            "description"=> $this->faker->sentence(4),
            "slug"=> Str::slug($name),
            "price"=> $this->faker->numerify('###').'.'.rand(0,9),
            "pricebase"=> $this->faker->numerify('##').'.'.rand(0,9),
            "isc"=> $this->faker->numerify('##').'.'.rand(0,9),
            "igv"=> 1,
            "stock"=> 1,
            "status"=> 1,
            "path_image"=> 'sin-foto.jpg',
            "branchoffice_id"=> 1,
        ];
    }
}
