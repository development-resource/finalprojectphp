<?php

namespace Database\Factories;

use App\Models\Orderheader;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderdetailFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $product = Product::all()->random();
        return [
            "canty"=> rand(1,6),
            "product_name"=> $product->name,
            "price"=> $product->price,
            "discount"=> 0,
            "order"=> 1,
            "orderheader_id"=> Orderheader::all()->random()->id,
            "product_id"=> $product->id,
        ];
    }
}
