<?php

namespace Database\Factories;

use App\Models\Statu;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class OrderheaderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->sentence(6);
        return [
            "num_fact"=>'fac-'.rand(10,99).$this->faker->numerify('######').rand(10,20),
            "slug"=>Str::slug($name),
            "delivery_day"=>$this->faker->dateTimeBetween('this week', '+5 days'), //Carbon::now(),
            "delivery_time"=> $this->faker->time('H:i'),
            "discount"=>$this->faker->numerify('#').'.'.rand(0,9),
            "status"=> Statu::all()->random()->id,
            "docu_link_cdr"=> Str::slug($name.'.cdr'),
            "docu_link_xml"=>Str::slug($name.'.xml'),
            "empr_nroruc"=>rand(10,99).$this->faker->numerify('#######').rand(10,88),
            "codigo_guia"=>'guia-'.rand(10,99).$this->faker->numerify('####').rand(10,20),
            "fecha_guia"=> $this->faker->dateTimeBetween('this week', '+20 days'),

        ];
    }
}
