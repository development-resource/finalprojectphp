<?php

namespace Database\Seeders;

use App\Models\Branchoffice;
use App\Models\Enterprice;
use Illuminate\Database\Seeder;

class BranchofficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branchoffice::create([
            "code" => "564212",
            "name" => "artifun principal",
            "contact" => "soporte almacen",
            "enterprice_id" => Enterprice::all()->random()->id,

        ]);
    }
}
