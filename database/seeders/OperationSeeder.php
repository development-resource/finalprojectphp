<?php

namespace Database\Seeders;

use App\Models\Operation;
use Illuminate\Database\Seeder;

class OperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Operation::create(
            [
                "code"=>"01",
                "name"=>"venta",
            ]
        );

        Operation::create(
            [
                "code"=>"02",
                "name"=>"compra",
            ]
        );

        Operation::create(
            [
                "code"=>"05",
                "name"=>"Devolucion recibida",
            ]
        );

        Operation::create(
            [
                "code"=>"16",
                "name"=>"saldo inicial",
            ]
        );
        
    }
}
