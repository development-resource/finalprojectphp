<?php

namespace Database\Seeders;

use App\Models\Enterprice;
use Illuminate\Database\Seeder;

class EnterpriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Enterprice::create([
            "name"=> "artifun",
            "comercialname"=> "Distribuidora",
            "document"=> "3215641212",
            "address"=> "av la marina",
            "phone"=> "987654564",
            "status"=> "1",
            "logo"=> "logo.jpg",
        ]);
    }
}
