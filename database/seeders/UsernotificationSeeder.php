<?php

namespace Database\Seeders;

use App\Models\Usernotification;
use Illuminate\Database\Seeder;

class UsernotificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usernotification::factory(60)->create();
    }
}
