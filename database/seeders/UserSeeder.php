<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name"=> "Thonny",
            "email"=> "admin@gmail.com",
            "phone"=> "987654321",
            "address"=> "av madre de dios",
            
            "password"=> bcrypt('1234'),
            "profile_photo_path"=> "qflores.jpg",
            "role_id"=> 1,
        ]);

        User::create([
            "name"=> "Thonny",
            "email"=> "thonn776@gmail.com",
            "phone"=> "987654321",
            "address"=> "av madre de dios",
            
            "password"=> bcrypt('qf1234'),
            "profile_photo_path"=> "qflores.jpg",
            "role_id"=> 1,
        ]);

        User::create([
            "name"=> "Freddy",
            "email"=> "freddy@gmail.com",
            "phone"=> "987654321",
            "address"=> "av santiagod e surco",
            
            "password"=> bcrypt('1234'),
            "profile_photo_path"=> "freddy.jpg",
            "role_id"=> 1,
        ]);

        User::create([
            "name"=> "artifun admin",
            "email"=> "artifunad@gmail.com",
            "phone"=> "658654321",
            "address"=> "av la republicas",
            
            "password"=> bcrypt('arf1234'),
            "profile_photo_path"=> "qflores.jpg",
            "role_id"=> 1,
        ]);

        User::create([
            "name"=> "almacen",
            "email"=> "almacen@gmail.com",
            "phone"=> "65954321",
            "address"=> "av chile 5642",
            
            "password"=> bcrypt('1234'),
            "profile_photo_path"=> "almacen.jpg",
            "role_id"=> 2,
        ]);

        User::create([
            "name"=> "ventas",
            "email"=> "ventas@gmail.com",
            "phone"=> "986554321",
            "address"=> "av chile 5642",
            
            "password"=> bcrypt('1234'),
            "profile_photo_path"=> "almacen.jpg",
            "role_id"=> 3,
        ]);

    }
}
