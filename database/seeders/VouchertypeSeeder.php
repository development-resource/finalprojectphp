<?php

namespace Database\Seeders;

use App\Models\Vouchertype;
use Illuminate\Database\Seeder;

class VouchertypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vouchertype::create([
            "code"=>"00",
            "name"=>"otros",
        ]);

        Vouchertype::create([
            "code"=>"01",
            "name"=>"Factura",
        ]);


        Vouchertype::create([
            "code"=>"03",
            "name"=>"Boleta de venta",
        ]);
        
        Vouchertype::create([
            "code"=>"07",
            "name"=>"Nota de credito",
        ]);
    }
}
