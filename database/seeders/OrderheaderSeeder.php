<?php

namespace Database\Seeders;

use App\Models\Orderheader;
use App\Models\Orderstatu;
use App\Models\User;
use Illuminate\Database\Seeder;

class OrderheaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Orderheader::factory(50)->create()
        ->each(function ($orderh) {
            Orderstatu::create([
                "day_star"=>    $orderh->delivery_day,
                "time_star"=>   $orderh->delivery_time,
                "user_id"=>     User::all()->random()->id,
                "orderheader_id"=>$orderh->id,
                "statu_id"=>    $orderh->status,
            ]);
        });

        //return var_dump($orders->attributes);        

    }
}
