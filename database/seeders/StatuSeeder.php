<?php

namespace Database\Seeders;

use App\Models\Statu;
use Illuminate\Database\Seeder;

class StatuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Statu::create([
            "code"=>"1",
            "name"=>"Orden preparado",
            "order"=>1,
            "operation_id"=>1,
            "io"=>NULL,
            "color"=>"#4c5564",
        ]);

        Statu::create([
            "code"=>"2",
            "name"=>"Orden planeado", //Almacen esta preparando los productos
            "order"=>1,
            "operation_id"=>1,
            "io"=>NULL,
            "color"=>"#d87704",
        ]);

        Statu::create([
            "code"=>"3",
            "name"=>"Orden en almacen",
            "order"=>1,
            "operation_id"=>1,
            "io"=>NULL,
            "color"=>"#6f26da",
        ]);

        Statu::create([
            "code"=>"4",
            "name"=>"Orden entregado",
            "order"=>1,
            "operation_id"=>1,
            "io"=>1,
            "color"=>"#057758",
        ]);

        Statu::create([
            "code"=>"5",
            "name"=>"Orden por cobrar",
            "order"=>1,
            "operation_id"=>1,
            "io"=>NULL,
            "color"=>"#16a34a",
        ]);

        Statu::create([
            "code"=>"6",
            "name"=>"Orden finalizada",
            "order"=>1,
            "operation_id"=>1,
            "io"=> NULL,
            "color"=>"#0092ca",
        ]);

        Statu::create([
            "code"=>"7",
            "name"=>"orden devuelta",
            "order"=>1,
            "operation_id"=>2,
            "io"=>NULL,
            "color"=>"#05004e",
        ]);

        Statu::create([
            "code"=>"8",
            "name"=>"orden ingresado", //Procesamos en el el kardex
            "order"=>2,
            "operation_id"=>2,
            "io"=>0,
            "color"=>"#0092ca",
        ]);

        Statu::create([
            "code"=>"7",
            "name"=>"Compra registrada",
            "order"=>1,
            "operation_id"=>3,
            "io"=>NULL,
            "color"=>"#05004e",
        ]);

        Statu::create([
            "code"=>"8",
            "name"=>"Compra procesada", //Procesamos en el el kardex
            "order"=>2,
            "operation_id"=>3,
            "io"=>0,
            "color"=>"#0092ca",
        ]);

       
    }
}
