<?php

namespace Database\Seeders;

use App\Models\Karde;
use App\Models\Orderdetail;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OrderdetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Orderdetail::factory(600)->create()->each(function ($orderd) {
            
            $statusid = $orderd->orderheader->estado->io;
            
            $kardep = Karde::where('product_id','=',$orderd->product_id)->orderBy('id','desc')->first();

            if($statusid ==1){
                if(!$kardep)
                {
                    $price_base = ($orderd->price/1.18);//compra
                    Karde::create([
                        "buy_canty"     => $orderd->canty,
                        "buy_unit"      => $price_base,
                        "buy_total"     => $orderd->canty*$price_base,
                        "prom_canty"    => $orderd->canty,
                        "prom_unit"     => $price_base,
                        "prom_total"    => $orderd->canty*$price_base,
                        "day_reg"       => Carbon::now(),
                        "product_id"    => $orderd->product_id,
                    ]);
                }else{

                    $priceBase  = $orderd->price/1.18;
                    $cantyOld   = $kardep->prom_canty;
                    $promOld    = $kardep->prom_total;
                    $cantyNew   = $cantyOld + $orderd->canty;
                    $buyTotal   = $orderd->canty*$priceBase;
                    $priceNew   = ($promOld+ $buyTotal)/$cantyNew;

                    Karde::create([
                        "buy_canty"     => $orderd->canty,
                        "buy_unit"      => $priceBase,
                        "buy_total"     => $orderd->canty*$priceBase,
                        "prom_canty"    => $cantyNew,
                        "prom_unit"     => $priceNew,
                        "prom_total"    => $cantyNew*$priceNew,
                        "day_reg"       => Carbon::now(),
                        "product_id"    => $orderd->product_id,
                    ]);
                }
            }

            if($statusid ===0){
                if($kardep){
                    $priceOld   = $kardep->prom_unit;
                    $cantyOld   = $kardep->prom_canty;
                    $promOld    = $kardep->prom_total;
                    $newCanty = $cantyOld-$orderd->canty;

                    Karde::create([
                        "sales_canty"   => $orderd->canty,
                        "sales_unit"    => $priceOld,
                        "sales_total"   => $orderd->canty*$priceOld,
                        "prom_canty"    => $newCanty,
                        "prom_unit"     => $priceOld,
                        "prom_total"    => $newCanty*$priceOld,
                        "day_reg"       => Carbon::now(),
                        "product_id"    => $orderd->product_id,
                    ]);
                }
            }

        });
    }
}
