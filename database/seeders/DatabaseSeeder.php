<?php

namespace Database\Seeders;

use App\Models\Usernotification;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(RoleSeeder::class);
        $this->call(OperationSeeder::class);
        $this->call(VouchertypeSeeder::class);
        $this->call(EnterpriceSeeder::class);
        $this->call(BranchofficeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(StatuSeeder::class);
        $this->call(OrderheaderSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(OrderdetailSeeder::class);

        $this->call(NotificationSeeder::class);
        $this->call(UsernotificationSeeder::class);


    }
}
