<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKardesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kardes', function (Blueprint $table) {
            $table->id();
            $table->string('num_fac')->nullable();
            $table->decimal('buy_canty',11,2)->nullable();
            $table->decimal('buy_unit',11,2)->nullable();
            $table->decimal('buy_total',11,2)->nullable();
            $table->decimal('sales_canty',11,2)->nullable();
            $table->decimal('sales_unit',11,2)->nullable();
            $table->decimal('sales_total',11,2)->nullable();
            $table->decimal('prom_canty',11,2)->nullable();
            $table->decimal('prom_unit',11,2)->nullable();
            $table->decimal('prom_total',11,2)->nullable();
            $table->date('day_reg');
            $table->unsignedBigInteger('operation_id')->nullable();
            $table->unsignedBigInteger('vouchertype_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();

            $table->foreign('operation_id')
            ->references('id')
            ->on('operations')
            ->onDelete('set null');

            $table->foreign('vouchertype_id')
            ->references('id')
            ->on('vouchertypes')
            ->onDelete('set null');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kardes');
    }
}
