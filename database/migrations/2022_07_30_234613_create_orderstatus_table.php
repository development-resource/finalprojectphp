<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderstatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderstatus', function (Blueprint $table) {
            $table->id();
            $table->date('day_star');
            $table->time('time_star');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('orderheader_id');
            $table->unsignedBigInteger('statu_id')->nullable();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('set null');

            $table->foreign('orderheader_id')
            ->references('id')
            ->on('orderheaders')
            ->onDelete('cascade');

            $table->foreign('statu_id')
            ->references('id')
            ->on('status')
            ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderstatus');
    }
}
