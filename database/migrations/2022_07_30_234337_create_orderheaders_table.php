<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderheadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderheaders', function (Blueprint $table) {
            $table->id();
            $table->string('num_fact')->nullable();
            $table->string('slug')->nullable();
            $table->date('delivery_day')->nullable();
            $table->time('delivery_time')->nullable();
            $table->decimal('discount',11,2)->nullable();
            $table->char('status',2)->default(1);
            $table->string('docu_link_cdr')->nullable();
            $table->string('docu_link_xml')->nullable();
            $table->string('empr_nroruc')->nullable();
            $table->string('codigo_guia')->nullable();
            $table->date('fecha_guia')->nullable();
            $table->unsignedBigInteger('operation_id')->nullable();

            $table->foreign('operation_id')
            ->references('id')
            ->on('operations')
            ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderheaders');
    }
}
