<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku')->nullable();
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('slug')->nullable();
            $table->decimal('price',11,2)->nullable();
            $table->decimal('pricebase',11,2)->nullable();
            $table->decimal('isc',11,2)->nullable();
            $table->char('igv',1)->nullable(); //boolean
            $table->char('stock',1)->nullable(); //boolean
            $table->char('status',1)->default(1); //boolean
            $table->string('path_image')->nullable();
            $table->unsignedBigInteger('branchoffice_id')->nullable();

            $table->foreign('branchoffice_id')
            ->references('id')
            ->on('branchoffices')
            ->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
