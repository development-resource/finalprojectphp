<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderdetails', function (Blueprint $table) {
            $table->id();
            $table->decimal('canty',11,2);
            $table->string('product_name')->nullable();
            $table->decimal('price',11,2);
            $table->decimal('discount',11,2)->nullable();
            $table->integer('order')->nullable();
            $table->unsignedBigInteger('orderheader_id');
            $table->unsignedBigInteger('product_id');

            $table->foreign('orderheader_id')
            ->references('id')
            ->on('orderheaders')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderdetails');
    }
}
