<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name');            
            $table->integer('order');
            $table->text('user_sms')->nullable();
            $table->boolean('io')->nullable(); //input 0 ->compra, output = 1 ->venta
            $table->string('color')->default('#059862');

            $table->unsignedBigInteger('operation_id');

            $table->foreign('operation_id')
            ->references('id')
            ->on('operations')
            ->onDelete('cascade');
            
            -
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status');
    }
}
