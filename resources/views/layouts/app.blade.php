<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'gestion de almacen de artifun') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        
        <!-- font awesome -->
        <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles
        <x-laravel-blade-sortable::scripts/>
        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
        
    </head>
    <body class="font-poppins antialiased :dark">

        <div class=" bg-gray-100">
            @livewire('navigation-menu')
            
            <div class="flex h-screen antialiased text-gray-900 bg-gray-100 dark:bg-dark dark:text-light" x-data="{isSidebarOpen: true, isActive: true, count: 0}"
            @resize.window="
            width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            if (width > 1024) {
                isSidebarOpen = true
            }else{
                isSidebarOpen = false
            }">

                <!-- Backdrop -->
                <div  @click="isSidebarOpen = !isSidebarOpen" class="fixed flex items-center space-x-4 top-14 right-4 lg:hidden z-30" aria-hidden="true">
                    <span aria-hidden="true" class="mt-5">
                        <svg x-show="!isSidebarOpen" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                        </svg>
                        <svg x-show="isSidebarOpen" class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </span>
                </div>

                <!-- sidebar-->
                <aside x-show="isSidebarOpen"
                    x-transition:enter="transition-all transform duration-300 ease-in-out"
                    x-transition:enter-start="-translate-x-full opacity-0"
                    x-transition:enter-end="translate-x-0 opacity-100"
                    x-transition:leave="transition-all transform duration-300 ease-in-out"
                    x-transition:leave-start="translate-x-0 opacity-100"
                    x-transition:leave-end="-translate-x-full opacity-0"
                    x-ref="sidebar"
                    @keydown.escape="window.innerWidth <= 1024 ? isSidebarOpen = false : true"
                    tabindex="-1"
                    :class="[isSidebarOpen ? 'flex flex-shrink-0' : 'hidden']"
                    class="fixed inset-y-0 flex flex-shrink-0 overflow-hidden bg-white border-r  lg:static dark:border-indigo-800 dark:bg-darker focus:outline-none shadow-xl shadow-gray-500/50" style="z-index: 13">
                        
                    <!-- Sidebar links -->
                    <nav   aria-label="Main" class="flex-1 w-64 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto ">
                        <!-- Dashboards links -->
                        <div x-data="{ isActive: false, open: false }">

                            <div class="space-y-6 md:space-y-5 mt-3">
                                <h1 class="font-bold text-4xl text-center md:hidden">
                                    <span class="text-teal-600">Amin</span>
                                </h1>
                                <h1 class="hidden md:block font-bold text-sm md:text-xl text-center">
                                    <span class="text-teal-600">Amin</span>
                                </h1>
                                <div id="profile" class="space-y-3">
                                    <img src="https://pbs.twimg.com/profile_images/1467997254929854470/mDYbXoVl_400x400.jpg" alt="Avatar user" class="w-10 md:w-16 rounded-full mx-auto"/>
                                    <div>
                                        <h2 class="font-medium text-xs md:text-sm text-center text-teal-500">
                                            {{ Auth::user()->name }}
                                        </h2>
                                        <p class="text-xs text-gray-500 text-center">Gestión de almacen</p>
                                    </div>
                                </div>
                                <div class="flex border-2 border-gray-200 rounded-md focus-within:ring-2 ring-teal-500">
                                    <input type="text" class="w-full rounded-tl-md rounded-bl-md px-2 py-3 text-sm text-gray-600 focus:outline-none" placeholder="buscar"/>
                                    <button class="rounded-tr-md rounded-br-md px-2 py-3 hidden md:block">
                                        <svg class="w-4 h-4 fill-current" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"> <path   fill-rule="evenodd"   d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"   clip-rule="evenodd" ></path>
                                        </svg>
                                    </button>
                                </div>
                                <div id="menu" class="flex flex-col space-y-2">
                                    <a  href="{{ route('dashboard') }}"  class="{{ (request()->is('dashboard*')) ? 'expand bg-teal-500 text-white' : '' }}  font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 hover:text-base rounded-md transition duration-150 ease-in-out">
                                        <i class="fas fa-tachometer-alt text-md"></i>
                                        <span class="text-sm">Dashboard</span>
                                    </a>

                                    <a  href="{{ route('products') }}"  class="{{ (request()->is('products*')) ? 'expand bg-teal-500 text-white' : '' }} text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path  d="M11 17a1 1 0 001.447.894l4-2A1 1 0 0017 15V9.236a1 1 0 00-1.447-.894l-4 2a1 1 0 00-.553.894V17zM15.211 6.276a1 1 0 000-1.788l-4.764-2.382a1 1 0 00-.894 0L4.789 4.488a1 1 0 000 1.788l4.764 2.382a1 1 0 00.894 0l4.764-2.382zM4.447 8.342A1 1 0 003 9.236V15a1 1 0 00.553.894l4 2A1 1 0 009 17v-5.764a1 1 0 00-.553-.894l-4-2z"></path>
                                        </svg>
                                        <span class="">Productos</span>
                                    </a>

                                    <a href="{{ route('graficas') }}" class="{{ (request()->is('graficas*')) ? 'expand bg-teal-500 text-white' : '' }} text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"
                                        >
                                        <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z"></path>
                                        <path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z" clip-rule="evenodd"
                                        ></path>
                                        </svg>
                                        <span class="">Reportes</span>
                                    </a>
                                    <a   href="{{ route('messages') }}"  class="{{ (request()->is('messages*')) ? 'expand bg-teal-500 text-white' : '' }} text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2 5a2 2 0 012-2h7a2 2 0 012 2v4a2 2 0 01-2 2H9l-3 3v-3H4a2 2 0 01-2-2V5z"></path>
                                        <path d="M15 7v2a4 4 0 01-4 4H9.828l-1.766 1.767c.28.149.599.233.938.233h2l3 3v-3h2a2 2 0 002-2V9a2 2 0 00-2-2h-1z"></path>
                                        </svg>
                                        <span class="">Mensajes</span>
                                    </a>
                                    <a  href="" class="text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M6 2a1 1 0 00-1 1v1H4a2 2 0 00-2 2v10a2 2 0 002 2h12a2 2 0 002-2V6a2 2 0 00-2-2h-1V3a1 1 0 10-2 0v1H7V3a1 1 0 00-1-1zm0 5a1 1 0 000 2h8a1 1 0 100-2H6z" clip-rule="evenodd"></path>
                                        </svg>
                                        <span class="">Calendario</span>
                                    </a>
                                    <a  href=""   class="text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M3 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zm0 4a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"
                                        ></path>
                                        </svg>
                                        <span class="">Tablas</span>
                                    </a>
                                    <a   href="{{ route('componentes') }}"  class="{{ (request()->is('componentes*')) ? 'expand bg-teal-500 text-white' : '' }} text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M5 3a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2V5a2 2 0 00-2-2H5zM5 11a2 2 0 00-2 2v2a2 2 0 002 2h2a2 2 0 002-2v-2a2 2 0 00-2-2H5zM11 5a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V5zM14 11a1 1 0 011 1v1h1a1 1 0 110 2h-1v1a1 1 0 11-2 0v-1h-1a1 1 0 110-2h1v-1a1 1 0 011-1z"
                                        ></path>
                                        </svg>
                                        <span class="">Componentes</span>
                                    </a>
                                    <a  href="{{ route('users') }}"  class="{{ (request()->is('users*')) ? 'expand bg-teal-500 text-white' : '' }} text-sm font-medium text-gray-700 py-2 px-2 hover:bg-teal-500 hover:text-white hover:scale-105 rounded-md transition duration-150 ease-in-out">
                                        <svg class="w-6 h-6 fill-current inline-block" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z"
                                        ></path>
                                        </svg>
                                        <span class="">Usuarios</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <!-- Components links -->
                        <div x-data="{ isActive: false, open: true }">
                            <a href="#" @click="$event.preventDefault(); open = !open" class="{{ (request()->is('configuracion*')) ? 'expand bg-teal-500 text-white' : '' }} flex items-center p-2 text-gray-500 hover:text-white transition-colors rounded-md dark:text-light hover:bg-teal-600 dark:hover:bg-teal-600" :class="{ 'bg-teal-500 dark:bg-teal-600 text-white': isActive || open }" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                                <span aria-hidden="true">
                                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"/>
                                    </svg>
                                </span>
                                <span class="ml-2 text-sm"> Configuraciones </span>
                                <span aria-hidden="true" class="ml-auto">
                                    <svg class="w-4 h-4 transition-transform transform" :class="{ 'rotate-180': open }" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                                    </svg>
                                </span>
                            </a>
                            <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" arial-label="Components">
                                <a href="{{ route('configservicie') }}" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700">
                                    Tipo de servicios
                                </a>
                                <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700">
                                    Configuraciones
                                </a>
                            </div>
                        </div>

                        <!-- Components links -->
                        <div x-data="{ isActive: false, open: false }">
                            <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 hover:text-white transition-colors rounded-md dark:text-light hover:bg-teal-600 dark:hover:bg-teal-600" :class="{ 'bg-teal-500 dark:bg-teal-600 text-white': isActive || open }" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                                <span aria-hidden="true">
                                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2V6zM14 6a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2V6zM4 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2H6a2 2 0 01-2-2v-2zM14 16a2 2 0 012-2h2a2 2 0 012 2v2a2 2 0 01-2 2h-2a2 2 0 01-2-2v-2z"/>
                                    </svg>
                                </span>
                                <span class="ml-2 text-sm"> Components </span>
                                <span aria-hidden="true" class="ml-auto">
                                    <svg class="w-4 h-4 transition-transform transform" :class="{ 'rotate-180': open }" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" >
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                                    </svg>
                                </span>
                            </a>
                            <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" arial-label="Components">
                                <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700" > Alertas
                                </a>
                                <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:text-gray-400 dark:hover:text-light hover:text-gray-700" > Configuraciones
                                </a>
                            </div>
                        </div>
                    </nav>
                </aside>

                <!-- Page Content -->
                <main class="flex-1 ">
                    <div class="flex flex-col flex-1 h-full min-h-screen p-4 overflow-x-hidden overflow-y-auto">
                        <div class="mt-14"></div>
                        {{ $slot }}
                    </div>
                </main>
            
                @stack('modals')

                

            </div>
        </div>
        

        {{-- tabla de livewire --}}
        <style>
            [x-cloak] { display: none !important; }

            ::-webkit-scrollbar {
                width: 8px;
            }

            ::-webkit-scrollbar-track {
                background: #fafafa;
            }

            ::-webkit-scrollbar-thumb {
                background-color: #bcc6d1;/* #cbd5e1; 424f64*/
                border-radius: 5px;
                border: 1px solid #ffffff;
            }

        </style>
        
        

        @livewireScripts

    </body>
</html>
