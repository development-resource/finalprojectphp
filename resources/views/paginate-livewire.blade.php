@if ($paginator->hasPages())
    <nav role="navigation" aria-label="{{ __('Pagination Navigation') }}" class="flex items-center justify-between">
        <div class="flex justify-between flex-1 sm:hidden">
            @if ($paginator->onFirstPage())
                <span class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                    Anterior
                </span>
            @else
                <button wire:click="previousPage" class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-100 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    Anterior
                </button>
            @endif

            @if ($paginator->hasMorePages())
                <button wire:click="nextPage" class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    Siguiente
                </button>
            @else
                <span class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-200 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                    Siguiente
                </span>
            @endif
        </div>

        <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
            <div>
                <p class="text-xs text-gray-700">
                        Mostrando
                    @if ($paginator->firstItem())
                        <span class="font-light">{{$paginator->firstItem()}}</span>
                        -
                        <span class="font-light">{{$paginator->lastItem()}}</span>
                    @else
                        {{ $paginator->count() }}
                    @endif
                        de
                    <span class="font-light">{{ $paginator->total() }}</span>
                </p>
            </div>

            <div class="flex my-auto items-center content-center">
                <span class="relative z-0 flex shadow-sm rounded-md my-auto items-center content-center border border-blue-200">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <span aria-disabled="true" aria-label="{{ __('pagination.previous') }}" class="my-auto w-6 h-6 bg-white border border-gray-300 cursor-default rounded-l-md leading-5 overflow-hidden">
                            <span class="relative inline-flex items-center text-sm font-medium text-gray-200 bg-white " aria-hidden="true">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @else
                        <button wire:click="previousPage" rel="prev" class="relative inline-flex items-center p-1 w-6 h-6 text-sm font-medium text-blue-500 bg-white border border-gray-300 rounded-l-md leading-5 hover:text-green-500 hover:font-bold focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-500 active:bg-gray-100 active:text-pink-500 transition ease-in-out duration-150" aria-label="{{ __('pagination.previous') }}">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    @endif

                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        @if (is_string($element))
                            <span aria-disabled="true" class="w-6 h-6 border border-gray-300 text-red-500 bg-white flex justify-center items-center text-center">
                                <span class="text-sm font-medium w-full h-full">{{ $element }}</span>
                            </span>
                        @endif

                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <span aria-disabled="true" class="w-6 h-6 border border-gray-300 text-gray-100 bg-blue-500 flex justify-center items-center text-center">
                                        <div class="text-sm font-medium  w-full h-full">
                                            {{ $page }} 
                                        </div>
                                    </span>
                                @else
                                    {{-- @if ($paginator->currentPage() < $page) --}}
                                        <button wire:click="gotoPage({{ $page }})" class="relative inline-flex items-center p-1 w-6 h-6 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-blue-500" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                            {{ $page }} 
                                        </button>
                                    {{-- @else
                                        <button wire:click="previousPage" class="relative inline-flex items-center p-1 w-6 h-6 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-blue-500" aria-label="{{ __('Go to page :page', ['page' => $page]) }}">
                                            {{ $page }} 
                                        </button>
                                    @endif --}}
                                @endif
                            @endforeach
                        @endif
                    @endforeach

                    @if ($paginator->hasMorePages())
                        <button wire:click="nextPage" rel="next" class="relative inline-flex items-center p-1 w-6 h-6 -ml-px text-sm font-medium text-blue-500 bg-white border border-gray-300 rounded-r-md leading-5 hover:text-green-500 hover:font-bold focus:outline-none focus:ring ring-gray-300 focus:border-blue-500 active:bg-gray-100 active:text-pink-500 transition ease-in-out duration-150" aria-label="{{ __('pagination.next') }}">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    @else
                        <span aria-disabled="true" aria-label="{{ __('pagination.next') }}" class="my-auto w-6 h-6 bg-white border border-gray-300 cursor-default rounded-r-md leading-5">
                            <span class="relative inline-flex items-center -ml-px text-sm font-medium text-gray-200 " aria-hidden="true">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>
                    @endif
                </span>
            </div>
        </div>
    </nav>
@endif