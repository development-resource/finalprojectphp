<x-app-layout>

    <div class="relative w-full">
    
        <livewire:kardex :product_id="$product_id" />

    </div>
</x-app-layout>