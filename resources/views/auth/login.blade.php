<x-guest-layout>
    
   
    <div class="bg-white dark:bg-gray-900">
        <div class="flex justify-center h-screen">
            <div class="hidden bg-cover lg:block lg:w-2/3" style="background-image: url({{ asset('img/login-background.jpg') }}); background-size: cover; object-fit: fill; ">
                <div class="flex items-center h-full px-20 bg-gray-900 bg-opacity-40">
                    <div>
                        <div class="text-4xl  text-extrabold text-white  items-center justify-center"
                        x-data="animation()"
                        x-init="animate()"
                        >
                            <template x-for="(c, i) in text.split('')" class="letterart">
                                <span
                                    x-text="c"
                                    class="opacity-0 transition delay-75 ease-in"
                                    :class="{'opacity-100':char>=i}">
                                </span
                            ></template>
                            
                        </div>
                        
                        <p class="max-w-xl mt-3 text-gray-300">Lorem ipsum dolor sit, amet consectetur adipisicing elit. In autem ipsa, nulla laboriosam dolores, repellendus perferendis libero suscipit nam temporibus molestiae</p>
                    </div>
                </div>
            </div>
            
            <div class="flex items-center w-full max-w-md px-6 mx-auto lg:w-2/6">
                <div class="flex-1">
                    <div class="text-center">
                        <div class="mx-auto rounded-xl text-center ">
                            <img class="h-28 w-28 mx-auto rounded-xl" src="{{ asset('img/logoalmacen.jpg') }}" alt="" style="height: 150px; width: 150px;">
                        </div>
                        
                       
                        <h2 class="text-4xl font-bold text-center text-gray-700 dark:text-white">ARTIFUN LOGIN</h2>
                        <p class="mt-3 text-gray-500 dark:text-gray-300">Ingrese sus credenciales</p>
                    </div>
                    <div class="">
                        <x-jet-validation-errors class="mb-4" />
                    </div>

                    <div class="mt-8">
                        <form class="" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div>
                                <label for="email" class="block mb-2 text-sm text-gray-600 dark:text-gray-200">Correo electrónico</label>
                                <input type="email" name="email" id="email" placeholder="example@example.com" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-200 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-400 dark:focus:border-blue-400 focus:ring-blue-400 focus:outline-none focus:ring focus:ring-opacity-40" />                                
                            </div>

                            <div class="mt-6">
                                <label for="password" class="text-sm text-gray-600 dark:text-gray-200">Contraseña</label>
                                <input type="password" name="password" id="password" placeholder="Your Password" class="block w-full px-4 py-2 mt-2 text-gray-700 placeholder-gray-400 bg-white border border-gray-200 rounded-md dark:placeholder-gray-600 dark:bg-gray-900 dark:text-gray-300 dark:border-gray-700 focus:border-blue-400 dark:focus:border-blue-400 focus:ring-blue-400 focus:outline-none focus:ring focus:ring-opacity-40" />                                
                            </div>

                            <div class="mt-6">
                                <button type="submit"
                                    class="w-full px-4 py-2 tracking-wide text-white transition-colors duration-200 transform bg-blue-500 rounded-md hover:bg-blue-400 focus:outline-none focus:bg-blue-400 focus:ring focus:ring-blue-300 focus:ring-opacity-50">
                                    Iniciar sesión
                                </button>
                            </div>

                        </form>

                        <div class="pt-5">
                            <div class="my-5 mb-2">
                                
                                <a href="{{ route('password.request') }}" class="text-sm text-gray-400 focus:text-blue-500 hover:text-blue-500 hover:underline">olvidó su contraseña?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function animation() {
           

            return {
                text: "ARTIFUN TEAM",
                char: -1,
                animate() {
                    let timer = setInterval(() => {
                        this.char++;
                        if (this.char == this.text.length) {
                            // clearInterval(timer);
                            // timer = null;
                            if(this.char == this.text.length){
                                this.char= -2;
                            }                        
                            return;
                        }                        
                    }, 250);                    
                }
            };
        }


    </script>
</x-guest-layout>
