<x-app-layout>

    <div class="">
    
        <h1 class="ml-4  text-xl font-semibold">Dashboard</h1>
                
        <div class="h-full ">

            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 p-4 gap-4">
                <div class="w-full">
                    <div class="bg-gradient-to-b from-green-200 to-green-100 border-b-4 border-green-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4 my-auto">
                                <div class="rounded-full p-1 bg-green-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-wallet fa-2x fa-inverse "></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">$3249 <span class="text-green-500"><i class="fas fa-caret-up"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Revenue</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full">
                    <div class="bg-gradient-to-b from-pink-200 to-pink-100 border-b-4 border-pink-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4 my-auto">
                                <div class="rounded-full p-1 bg-pink-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-users fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center ">
                                <div class="">
                                    <p class="">$3249 <span class="text-pink-500"><i class="fas fa-exchange-alt"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Users</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="w-full">
                    <div class="bg-gradient-to-b from-yellow-200 to-yellow-100 border-b-4 border-yellow-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-yellow-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-user-plus fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">2<span class="text-yellow-600"><i class="fas fa-caret-up"></i></span></p>
                                    <h2 class="uppercase text-gray-600 text-xs">New Users</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full">
                    <div class="bg-gradient-to-b from-blue-200 to-blue-100 border-b-4 border-blue-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-blue-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-server fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">152</p>
                                    <h2 class="uppercase text-gray-600 text-xs">Server Uptime</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="relative bg-white text-xs border shadow-lg rounded-md p-2 gap-2 text-black dark:text-white" style="z-index: 12">
                <div class="px-2 bg-gray-100 py-1 w-full rounded-sm">
                    <h3 class="text-lg font-semibold">Lista de órdenes</h3>
                </div>
                <div class="">
                    <livewire:listorders />
                    
                </div>
            </div>

            
            <div class="grid grid-cols-1 sm:grid-cols-2 p-4 gap-4">
        
                <div class="relative flex flex-col min-w-0 mb-4 lg:mb-0 break-words bg-white dark:bg-gray-800 w-full shadow-lg rounded">
                    <div class="rounded-t mb-0 px-0 border-0">
                        <div class="flex flex-wrap items-center px-4 py-2">
                            <div class="relative w-full max-w-full flex-grow flex-1">
                                <h3 class="font-semibold text-base text-gray-900 dark:text-gray-50">Lista de productos</h3>
                            </div>
                            <div class="relative w-full max-w-full flex-grow flex-1 text-right">
                                <button class="bg-blue-500 dark:bg-gray-100 text-white active:bg-blue-600 dark:text-gray-800 dark:active:text-gray-700 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button">
                                Ver todo
                                </button>
                            </div>
                        </div>
                        <div class="block w-full overflow-x-auto">
                            <table class="items-center w-full bg-transparent border-collapse">
                                <thead>
                                    <tr class="bg-gray-200">
                                        <th class="px-4 dark:bg-gray-600 text-gray-500 dark:text-gray-100 align-middle dark:border-gray-500 py-3 text-xs uppercase whitespace-nowrap font-semibold text-left">
                                            Nombre
                                        </th>
                                        <th class="px-4 dark:bg-gray-600 text-gray-500 dark:text-gray-100 align-middle dark:border-gray-500 py-3 text-xs uppercase whitespace-nowrap font-semibold text-left">
                                            Precio
                                        </th>
                                        <th class="px-4 dark:bg-gray-600 text-gray-500 dark:text-gray-100 align-middle dark:border-gray-500 py-3 text-xs uppercase whitespace-nowrap font-semibold text-left">
                                            Movimiento
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @if ($productos)
                                        @foreach ($productos as $producto)
                                            <tr class="text-gray-700 dark:text-gray-100 hover:bg-gray-200 border border-b">
                                                <td class="border-t-0 px-4 align-middle text-xs whitespace-nowrap text-left p-2">{{ $producto->name }}</td>
                                                <td class="border-t-0 px-4 align-middle text-xs whitespace-nowrap">{{ $producto->price }}</td>
                                                <td class="border-t-0 px-4 align-middle text-xs whitespace-nowrap">
                                                    <div class="flex items-center">
                                                        <span class="mr-2">70%</span>
                                                        <div class="relative w-full">
                                                        <div class="overflow-hidden h-2 text-xs flex rounded bg-blue-200">
                                                            <div style="width: 70%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-blue-600"></div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                   
                                </tbody>

                                <tfoot>
                                    <tr class="font-semibold text-gray-900 dark:text-white">
                                        <td colspan="3" class="py-2 px-3 ">{{ $productos->onEachSide(1)->links('paginate') }}</td>
                                    </tr>
                                </tfoot>
                                
                            </table>
                        </div>
                    </div>
                </div>
        
                <div class="relative flex flex-col min-w-0 break-words bg-gray-50 dark:bg-gray-800 w-full shadow-lg rounded z-10">
                    <livewire:usernotifications />
                </div>
            </div>
        
            
            {{-- <div class="grid grid-cols-1 p-4 gap-4 text-black dark:text-white">
                <div class="md:col-span-2 xl:col-span-3">
                    <h3 class="text-lg font-semibold">Actividades recientes</h3>
                </div>
                <div class="">
                    <livewire:orderheader-table />
                </div>
            </div> --}}

             {{-- <div class="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 p-4 gap-4 text-black dark:text-white">
                <div class="md:col-span-2 xl:col-span-3">
                    <h3 class="text-lg font-semibold">Actividades recientes</h3>
                </div>
                <div class="md:col-span-2 xl:col-span-1">
                    <div class="rounded bg-gray-200 dark:bg-gray-800 p-3">
                        <div class="flex justify-between py-1 text-black dark:text-white">
                        <h3 class="text-sm font-semibold">Tareas por hacer</h3>
                        <svg class="h-4 fill-current text-gray-600 dark:text-gray-500 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z" /></svg>
                        </div>
                        <div class="text-sm text-black dark:text-gray-50 mt-2">
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Delete all references from the wiki</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Remove analytics code</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">
                            Diseños
                            <div class="text-gray-500 dark:text-gray-200 mt-2 ml-2 flex justify-between items-start">
                            <span class="text-xs flex items-center">
                                <svg class="h-4 fill-current mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M11 4c-3.855 0-7 3.145-7 7v28c0 3.855 3.145 7 7 7h28c3.855 0 7-3.145 7-7V11c0-3.855-3.145-7-7-7zm0 2h28c2.773 0 5 2.227 5 5v28c0 2.773-2.227 5-5 5H11c-2.773 0-5-2.227-5-5V11c0-2.773 2.227-5 5-5zm25.234 9.832l-13.32 15.723-8.133-7.586-1.363 1.465 9.664 9.015 14.684-17.324z" /></svg>
                                3/5
                            </span>
                            <img src="https://i.imgur.com/OZaT7jl.png" class="rounded-full" />
                            </div>
                        </div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Check the meta tags</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">
                            Ejemplos
                            <div class="text-gray-500 dark:text-gray-200 mt-2 ml-2 flex justify-between items-start">
                            <span class="text-xs flex items-center">
                                <svg class="h-4 fill-current mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M11 4c-3.855 0-7 3.145-7 7v28c0 3.855 3.145 7 7 7h28c3.855 0 7-3.145 7-7V11c0-3.855-3.145-7-7-7zm0 2h28c2.773 0 5 2.227 5 5v28c0 2.773-2.227 5-5 5H11c-2.773 0-5-2.227-5-5V11c0-2.773 2.227-5 5-5zm25.234 9.832l-13.32 15.723-8.133-7.586-1.363 1.465 9.664 9.015 14.684-17.324z" /></svg>
                                0/3
                            </span>
                            </div>
                        </div>
                        <p class="mt-3 text-gray-600 dark:text-gray-400">Add a card...</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="rounded bg-gray-200 dark:bg-gray-800 p-3">
                        <div class="flex justify-between py-1 text-black dark:text-white">
                        <h3 class="text-sm font-semibold">Desarrollos</h3>
                        <svg class="h-4 fill-current text-gray-600 dark:text-gray-500 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z" /></svg>
                        </div>
                        <div class="text-sm text-black dark:text-gray-50 mt-2">
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Delete all references from the wiki</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Remove analytics code</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">
                            Interfaces
                            <div class="flex justify-between items-start mt-2 ml-2 text-white text-xs">
                            <span class="bg-pink-600 rounded p-1 text-xs flex items-center">
                                <svg class="h-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 2c-.8 0-1.5.7-1.5 1.5v.688C7.344 4.87 5 7.62 5 11v4.5l-2 2.313V19h18v-1.188L19 15.5V11c0-3.379-2.344-6.129-5.5-6.813V3.5c0-.8-.7-1.5-1.5-1.5zm-2 18c0 1.102.898 2 2 2 1.102 0 2-.898 2-2z" /></svg>
                                2
                            </span>
                            </div>
                        </div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Check the meta tags</div>
                        <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">
                            Mis cosas
                            <div class="text-gray-500 mt-2 ml-2 flex justify-between items-start">
                            <span class="text-xs flex items-center">
                                <svg class="h-4 fill-current mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M11 4c-3.855 0-7 3.145-7 7v28c0 3.855 3.145 7 7 7h28c3.855 0 7-3.145 7-7V11c0-3.855-3.145-7-7-7zm0 2h28c2.773 0 5 2.227 5 5v28c0 2.773-2.227 5-5 5H11c-2.773 0-5-2.227-5-5V11c0-2.773 2.227-5 5-5zm25.234 9.832l-13.32 15.723-8.133-7.586-1.363 1.465 9.664 9.015 14.684-17.324z" /></svg>
                                0/3
                            </span>
                            </div>
                        </div>
                        <p class="mt-3 text-gray-600 dark:text-gray-400">Agregar card</p>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="rounded bg-gray-200 dark:bg-gray-800 p-3">
                        <div class="flex justify-between py-1 text-black dark:text-white">
                            <h3 class="text-sm font-semibold">Tareas</h3>
                         <svg class="h-4 fill-current text-gray-600 dark:text-gray-500 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 10a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4zm7 0a1.999 1.999 0 1 0 0 4 1.999 1.999 0 1 0 0-4z" /></svg>
                        </div>
                        <div class="text-sm text-black dark:text-gray-50 mt-2">
                            <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Delete all references from the wiki</div>
                            <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Remove analytics code</div>
                            <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Do a mobile first layout</div>
                            <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">Check the meta tags</div>
                            <div class="bg-white dark:bg-gray-600 hover:bg-gray-50 dark:hover:bg-gray-700 p-2 rounded mt-1 border-b border-gray-100 dark:border-gray-900 cursor-pointer">
                                Cosas
                                <div class="text-gray-500 dark:text-gray-200 mt-2 ml-2 flex justify-between items-start">
                                <span class="text-xs flex items-center">
                                    <svg class="h-4 fill-current mr-1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M11 4c-3.855 0-7 3.145-7 7v28c0 3.855 3.145 7 7 7h28c3.855 0 7-3.145 7-7V11c0-3.855-3.145-7-7-7zm0 2h28c2.773 0 5 2.227 5 5v28c0 2.773-2.227 5-5 5H11c-2.773 0-5-2.227-5-5V11c0-2.773 2.227-5 5-5zm25.234 9.832l-13.32 15.723-8.133-7.586-1.363 1.465 9.664 9.015 14.684-17.324z" /></svg>
                                    0/3
                                </span>
                                </div>
                            </div>
                            <p class="mt-3 text-gray-600 dark:text-gray-400">Agregar</p>
                        </div>
                    </div>
                </div>
            </div> --}}
           
        </div>
    </div>

</x-app-layout>
