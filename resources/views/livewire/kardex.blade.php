<div>
    <div class="h-full">
        <h1 class="ml-4  text-xl font-semibold">Kardex por ponderación {{ $product_id }}</h1>
                
        <div class="">

            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 p-4 gap-4">
                <div class="w-full">
                    <div class="bg-gradient-to-b from-green-200 to-green-100 border-b-4 border-green-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto relative overflow-hidden">
                            <div class="flex-shrink pr-4 my-auto ">
                                <div class="rounded-full p-1 bg-green-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-wallet fa-2x fa-inverse "></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center z-10">
                                <div class="">
                                    <p class="">$3249 <span class="text-green-500"><i class="fas fa-caret-up"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Revenue</p>
                                </div>
                            </div>
                            <div class="absolute -bottom-10 right-1 h-20 w-20 rounded-full backdrop-opacity-20 bg-green-500/30 animate-ping duration-1000 ease-in-out"></div>
                        </div>
                    </div>
                </div>


                <div class="w-full">
                    <div class="bg-gradient-to-b from-pink-200 to-pink-100 border-b-4 border-pink-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4 my-auto">
                                <div class="rounded-full p-1 bg-pink-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-users fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center ">
                                <div class="">
                                    <p class="">$3249 <span class="text-pink-500"><i class="fas fa-exchange-alt"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Users</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="w-full">
                    <div class="bg-gradient-to-b from-yellow-200 to-yellow-100 border-b-4 border-yellow-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-yellow-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-user-plus fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">2<span class="text-yellow-600"><i class="fas fa-caret-up"></i></span></p>
                                    <h2 class="uppercase text-gray-600 text-xs">New Users</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full">
                    <div class="bg-gradient-to-b from-blue-200 to-blue-100 border-b-4 border-blue-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-blue-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-server fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">152</p>
                                    <h2 class="uppercase text-gray-600 text-xs">Server Uptime</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="py-2">
            <div class="relative animated fadeIn faster  flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover" style="background-image: url({{ asset('img/logisticback.jpg') }});">
                <div class="absolute bg-black opacity-40 inset-0 z-0"></div>
                    <div class="w-full bg-gray-900 bg-opacity-30 bg-transparent shadow-lg rounded-xl p-6">
                        <div class="grid grid-cols-1 sm:grid-cols-3 gap-2 ">
                            
                            <div class="relative  max-w-md  mb-3 overflow-hidden">
                                <div class="absolute flex flex-col top-0 right-0 p-3">
                                    <button class="transition ease-in duration-300 bg-gray-800  hover:text-purple-500 shadow hover:shadow-md text-gray-500 rounded-full w-8 h-8 text-center p-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z" />
                                        </svg>
                                    </button>
                                </div>
                                <img width="150" height="150" class="w-full rounded-2xl hover:scale-105 duration-1000" src="{{ asset('img/products.jpg') }}">
                            </div>

                            <div class="col-span-2 px-5 hover:drop-shadow-lg backdrop-opacity-20 backdrop-blur-md hover:backdrop-opacity-80 hover:backdrop-blur-2xl bg-gradient-to-tl from-gray-300/60 to-transparent w-full shadow-md rounded-md">
                                
                                <div class="pt-1">
                                    
                                    <div class="w-full flex-none text-sm flex items-center text-gray-600">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-yellow-500 mr-1" viewBox="0 0 20 20" fill="currentColor">
                                            <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                                        </svg>
                                        <span class="text-white whitespace-nowrap mr-3">4.60 View </span>
                                    </div>

                                    <div class="w-full text-sm text-gray-200 hover:text-white">
                                        <p class="">Código: {{ $product->code }}</p>  
                                        <p class="">
                                            Producto: {{ $product->name }}
                                        </p>
                                        <p class="">
                                            Detalle: {{ $product->description }}
                                        </p>     
                                                                                
                                    </div>
                                </div>

                                <div class="text-sm text-white border rounded-md border-gray-400 ">
                                    <div class="mb-3 ">
                                        <div class="w-full">
                                            <ul class="px-2 gap-2">
                                                @if (count($cantystatus)> 0)
                                                    @foreach ($cantystatus as  $status)
                                                        <li class="flex gap-2 my-1 hover:bg-slate-700 px-2 cursor-help">
                                                            <div class="pr-2">
                                                                <span class="block p-1 border-2 border-gray-300  rounded-full transition ease-in duration-300" style="border: solid 2px {{$status->color}};">
                                                                    <p class="border block w-3 h-3 bg-blue-600 rounded-full" style="background-color: {{ $status->color }}; "></p>
                                                                </span>
                                                            </div>
                                                            <div class="pr-2">
                                                                {{ $status->canty }}
                                                            </div>
                                                            <div class="">
                                                                {{ $status->name }}
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                    
                                                @endif
                                            </ul>
                                        </div>
                                    </div> 
                                                                          
                                </div>    
                                
                                <div class="font-bold text-xl text-gray-100  text-opacity-25 pt-2">
                                    Online Kardex
                                </div> 
                                
                            </div>                                
                        </div>
                    </div>
            </div>
        </div>
        {{-- lista deproductos --}}
        <div class="w-full bg-white rounded-xl shadow-xl ">
            <div class="w-full">
                
            </div>
            <div class="w-full max-h-96 overflow-scroll">
                <table class="w-full ">
                    <thead>
                        <tr class="bg-teal-500 text-gray-200 uppercase text-xs py-4 ">
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Canty C</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Price C</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Total C</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Canty S</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Price S</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Total S</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Canty P</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Price P</th>
                            <th class="top-0 sticky py-2 border-r border-white bg-teal-500">Total P</th>
                        </tr>
                    </thead>
                    <tbody class="text-gray-600 text-sm font-light text-left align-baseline" >
                        @if ( count($kardexes) > 0 )
                            @foreach ($kardexes as $kardex)
                            
                                <tr class="border-b border-gray-200 hover:bg-gray-100">
                                    
                                    <td class="py-3 px-6 text-left whitespace-nowrap">
                                        <div class="flex items-center">                                        
                                            <span class="font-medium">{{ $kardex->buy_canty }}</span>
                                        </div>
                                    </td>
            
                                    <td class="py-3 px-6 text-left">
                                        <div class=" ">
                                            <span>{{ $kardex->buy_unit }}</span>
                                        </div>
                                    </td>
            
                                    <td class="py-3 px-6  hidden sm:table-cell">
                                        <div class="">
                                            {{ $kardex->buy_total }}
                                        </div>
                                    </td>
            
                                    <td class="py-3 px-6  hidden sm:table-cell">
                                        <div class="">
                                            {{ $kardex->sales_canty }}
                                        </div>
                                    </td>
                                    <td class="py-3 px-6  hidden sm:table-cell">
                                        <div class="">
                                            {{ $kardex->sales_unit }}
                                        </div>
                                    </td>
                                    <td class="py-3 px-6  hidden sm:table-cell">
                                        <div class="">
                                            {{ $kardex->sales_total }}
                                        </div>
                                    </td>
                                    
                                    <td class="py-3 px-6 hidden md:table-cell">
                                        <div class="">
                                            {{ $kardex->prom_canty }}
                                        </div>
                                    </td>
            
                                    <td class="py-3 px-6 text-center hidden md:table-cell">
                                        <div class="">
                                            {{ $kardex->prom_unit }}
                                        </div>
                                    </td>
            
                                    <td class="py-3 px-6 text-center ">
                                        <div class="">
                                            {{ $kardex->prom_total }}
                                        </div>
                                    </td>                            
                                </tr>
                                
                            
                            @endforeach
    
    
                        @endif
                        
            
                    </tbody>
    
                   
    
                    <tfoot class="">
                        <tr>
                            <td colspan="9" class="bottom-0 sticky">
                                <div class="py-3 bg-teal-400 px-3 text-md rounded-b-xl">
                                    
                                   
    
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                    
    
                </table>
            </div>
            
        </div>
    </div>
       

</div>
