<div>
    <div class="">
        <div class="w-full py-2 px-2 flex justify-between  font-bold">
            <h3 class=" text-xl"> Lista de Roles</h3>
            <button class="bg-green-500 hover:bg-green-600 text-white px-2 py-1 rounded-md text-sm"><i class="fas fa-plus"></i> Agregar</button>
        </div>
        @if (count($roles)>0)
        
            @foreach ($roles as $role)
                <div class="bg-gray-50 rounded-md py-2 my-1" x-data="{open:{{ ($loop->index==0) ? 'true':'false'}}}">
                    <div class="flex justify-between items-center w-full px-2" @click="open =!open">
                        <div class="flex items-center focus:outline-none cursor-pointer ">
                            <svg x-show="open" class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M20 12H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                </path>
                            </svg>
                            <svg x-show="!open" class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12 4v16m8-8H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                </path>
                            </svg>
                            <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                {{ $role->name }}
                            </h1>
                        </div>
                        <div class="flex-shrink-0 right-0 gap-2">
                            <button class="bg-teal-500 hover:bg-teal-600 text-white px-2 py-1 rounded-md text-sm" >
                                Editar
                            </button>
                            <button class="bg-pink-500 hover:bg-pink-600 text-white px-2 py-1 rounded-md text-sm" >
                                Eliminar
                            </button>
                        </div>
                        
                    </div>
                    
                    <div class="flex mt-2 md:mx-10" x-show="open">
                        <span class="border border-blue-500">
                        </span>
                        <div class="py-1">
                            <p class="max-w-3xl px-4 text-gray-500 dark:text-gray-300">
                               <span class="font-bold"> Id:</span> {{ $role->id }}
                            </p>
                            <p class="max-w-3xl px-4 text-gray-500 dark:text-gray-300">
                                <span class="font-bold"> Nombre: </span> {{ $role->name }}
                            </p>
                            
                            <p class="max-w-3xl px-4 text-gray-500 dark:text-gray-300">
                                <span class="font-bold"> Fecha de creación: </span> {{ $role->created_at }}
                            </p>
                            <p class="max-w-3xl px-4 text-gray-500 dark:text-gray-300">
                                <span class="font-bold"> Fecha de actualización: </span> {{ $role->updated_at }}
                            </p>

                        </div>
                        
                        
                    </div>
                </div>
            @endforeach
            
        @else
            <div class="py-2 px-3 text-md font-bold">No hay lista de Coprobantes</div>
        @endif
       
    </div>
</div>
