<div>
    <section class="bg-white dark:bg-gray-900" style="z-index: 5">
        <div class="container px-3 py-12 mx-auto">
            <h1 class="text-sm font-bold text-center text-gray-800 lg:text-xl dark:text-white">
                Componentes Generales
            </h1>
            <div class="mt-4 flex relative">
                <div class="absolute sm:relative mt-6 mx-1 max-w-xs w-52 bg-gray-100 p-1 rounded-md shadow-md" 
                    x-data="{open: true}" 
                    @resize.window="
                    width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                    if (width > 640) {
                        open = true
                    }else{
                        open = false
                    }">
                    <h1 class="text-xl font-semibold text-gray-800 dark:text-white pt-4 text-center w-full hover:bg-gray-50 cursor-pointer" @click="open=!open">
                        Componentes
                    </h1>
                    <div class="mt-4 space-y-4 lg:mt-8 sm:block" :class="[open ? 'block flex-shrink-0' : 'hidden']" x-show="open">
                        <a class="hover:bg-white pl-1 py-2 block hover:text-blue-500 text-gray-600 dark:text-blue-400 cursor-pointer {{ $modul_id==1 ? 'bg-white text-blue-500':'' }}" wire:click="moduloId(1)">
                            <i class="fas fa-caret-right"></i> Comprobante de pago
                        </a>
                        <a class="hover:bg-white pl-1 py-2 block hover:text-blue-500 text-gray-600 dark:text-gray-300 cursor-pointer {{ $modul_id==2 ? 'bg-white text-blue-500':'' }}" wire:click="moduloId(2)">
                            <i class="fas fa-caret-right"></i> Roles
                        </a>
                        <a class="hover:bg-white pl-1 py-2 block hover:text-blue-500 text-gray-600 dark:text-gray-300 cursor-pointer {{ $modul_id==3 ? 'bg-white text-blue-500':'' }}" wire:click="moduloId(3)">
                            <i class="fas fa-caret-right"></i> Empresa
                        </a>
                        <a class="hover:bg-white pl-1 py-2 block hover:text-blue-500 text-gray-600 dark:text-gray-300 cursor-pointer {{ $modul_id==4 ? 'bg-white text-blue-500':'' }}" wire:click="moduloId(4)">
                            <i class="fas fa-caret-right"></i> Sucursal
                        </a>
                        <a class="hover:bg-white pl-1 py-2 block hover:text-blue-500 text-gray-600 dark:text-gray-300 cursor-pointer {{ $modul_id==5 ? 'bg-white text-blue-500':'' }}" wire:click="moduloId(5)">
                            <i class="fas fa-caret-right"></i> Otros
                        </a>
                    </div>
                </div>
                <div class="flex-1 mt-3 lg:mx-12 lg:mt-0">
                    @if ($modul_id==1)
                        <livewire:comprobantepago />
                    @elseif ($modul_id==2)
                        <livewire:rolesuser />
                    @elseif ($modul_id==3)
                        <div class="py-2 px-3 text-md font-bold">Componente no configurado</div>
                    @elseif ($modul_id==4)
                        <div class="py-2 px-3 text-md font-bold">Componente no configurado</div>
                    @elseif ($modul_id==5)
                        <div class="py-2 px-3 text-md font-bold">Componente no configurado</div>
                    @else
                        <div class="py-2 px-3 text-md font-bold">Componente no configurado</div>
                        {{-- <div class="border">
                            <div class="border">
                                <button class="flex items-center focus:outline-none">
                                    <svg class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M20 12H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                        </path>
                                    </svg>
                                    <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                        How i can play for my appoinment ?
                                    </h1>
                                </button>
                                <div class="flex mt-8 md:mx-10">
                                    <span class="border border-blue-500">
                                    </span>
                                    <p class="max-w-3xl px-4 text-gray-500 dark:text-gray-300">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni, eum quae. Harum officiis reprehenderit ex quia ducimus minima id provident molestias optio nam vel, quidem iure voluptatem, repellat et ipsa.
                                    </p>
                                </div>
                            </div>
                            <hr class="my-8 border-gray-200 dark:border-gray-700">
                                <div>
                                    <button class="flex items-center focus:outline-none">
                                        <svg class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12 4v16m8-8H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                            </path>
                                        </svg>
                                        <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                            What can i expect at my first consultation ?
                                        </h1>
                                    </button>
                                </div>
                                <hr class="my-8 border-gray-200 dark:border-gray-700">
                                    <div>
                                        <button class="flex items-center focus:outline-none">
                                            <svg class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M12 4v16m8-8H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                                </path>
                                            </svg>
                                            <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                                What are your opening house ?
                                            </h1>
                                        </button>
                                    </div>
                                    <hr class="my-8 border-gray-200 dark:border-gray-700">
                                        <div>
                                            <button class="flex items-center focus:outline-none">
                                                <svg class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M12 4v16m8-8H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                                    </path>
                                                </svg>
                                                <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                                    Do i need a referral ?
                                                </h1>
                                            </button>
                                        </div>
                                        <hr class="my-8 border-gray-200 dark:border-gray-700">
                                            <div>
                                                <button class="flex items-center focus:outline-none">
                                                    <svg class="w-6 h-6 text-blue-500" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M12 4v16m8-8H4" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                                                        </path>
                                                    </svg>
                                                    <h1 class="mx-4 text-xl text-gray-700 dark:text-white">
                                                        Is the cost of the appoinment covered by private health insurance ?
                                                    </h1>
                                                </button>
                                            </div>
                                        </hr>
                                    </hr>
                                </hr>
                            </hr>
                        </div> --}}
                    @endif
                    
                </div>
            </div>
        </div>
    </section>
</div>
