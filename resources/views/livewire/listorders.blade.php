<div>
    <div class="shadow-lg rounded-sm bg-white grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 py-1 px-1">
        
        <div class="my-auto">            
            <div wire:model.prevent="searchtext" class="flex items-center">   
                <label for="simple-search" class="sr-only">Buscar</label>
                <div class="relative w-full">
                    <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                        <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
                    </div>
                    <input wire:model.defer="search" type="text" class="h-7 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-l-lg focus:ring-1 active:ring-1 active:border-1 focus:ring-purple-500 focus:border-purple-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="buscar">
                </div>
                <button wire:click="searchtext" class=" h-7 px-2 py-1 text-sm font-medium text-white bg-purple-700 rounded-r-lg border border-purple-700 active:ring-1 hover:bg-blue-800 focus:ring-1 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                    <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                    <span class="sr-only">Buscar</span>
                </button>
            </div>
        </div>

        <div class="my-auto"></div>
        
        <div class="my-auto">
            <div class="space-x-8  ">
                {{-- contentClasses = para el body; dropdownClasses= div padre --}}
                <x-jet-dropdown align="left" width="48" class="w-full sm:w-52 " dropdownClasses="bg-gray-50 " contentClasses="overflow-auto h-72 relative max-w-sm mx-auto">
                    <x-slot name="trigger">
                        <button class="flex items-center w-full text-sm font-medium text-gray-500  hover:text-gray-700  hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div> <i class="fas fa-filter"></i> Filtros</div>
                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content">
                        <div class="block px-4 py-2 text-xs text-gray-400">
                            {{ __('Estados') }}
                        </div>
                        @if (count($estados)>0)
                            <p wire:click="statusall()" class="flex cursor-pointer justify-between px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-gray-100">
                                <span>Todo </span> 
                                <span> </span> 
                            </p>
                                                        
                            @foreach ($estados as $estado)
                                <p wire:click="filterStatu({{ $estado->id }})" class="flex cursor-pointer justify-between px-4 py-2 text-sm leading-5 text-gray-700 hover:text-white hover:bg-gray-100" onMouseOver="this.style.color='{{ $estado->color }}'">
                                    <span>{{ $estado->name }} </span> 
                                    <span class="right-0 ">
                                        @if ($estado->io === 1)
                                            <i class="fas fa-minus-circle text-pink-500"></i>
                                        @elseif ($estado->io === 0)
                                            <i class="fas fa-plus-circle text-green-500"></i>
                                        @else
                                            <i class=""></i>
                                        @endif
                                    </span>
                                </p>
                            @endforeach
                        @endif
                    </x-slot>
                </x-jet-dropdown>
            </div>
        </div>

        <div class="my-auto">
            <div class="space-x-8">
                <x-jet-dropdown align="left" width="48" class="w-full sm:w-52" >
                    <x-slot name="trigger" >
                        <button class="flex items-center text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
                            <div><i class="fas fa-sort-numeric-down"></i> Mostrar</div>
                            <div class="ml-1">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                </svg>
                            </div>
                        </button>
                    </x-slot>

                    <x-slot name="content" >
                        <div class="block px-4 py-2 text-xs text-gray-400">
                            {{ __('Registros') }}
                        </div>
                            <p wire:click="limitrows(1)" class="cursor-pointer block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-teal-500 hover:text-white">
                                All
                            </p>
                            <p wire:click="limitrows(10)" class="cursor-pointer block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-teal-500 hover:text-white">
                                10
                            </p>
                            <p wire:click="limitrows(30)" class="cursor-pointer block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-teal-500 hover:text-white">
                                30
                            </p>
                            <p wire:click="limitrows(100)" class="cursor-pointer block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-teal-500 hover:text-white">
                                100
                            </p>
                            <p wire:click="limitrows(200)" class="cursor-pointer block px-4 py-2 text-sm leading-5 text-gray-700 hover:bg-teal-500 hover:text-white">
                                200
                            </p>
                            
                    </x-slot>
                </x-jet-dropdown>
            </div>
        </div>

    </div>
    <div class=" overflow-x-scroll w-full">
        <table class="w-full table">
            <thead>
                <tr class="bg-gray-200 text-gray-600 uppercase text-xs ">
                    <th class="py-2 border-r border-white">Usuario</th>
                    <th class="py-2 border-r border-white">#Factura</th>
                    <th class="py-2 border-r border-white hidden sm:table-cell">Fecha entrega</th>
                    <th class="py-2 border-r border-white hidden sm:table-cell ">Items</th>
                    <th class="py-2 border-r border-white hidden md:table-cell">Estado</th>
                    <th class="py-2 border-r border-white hidden md:table-cell">Último actualización</th>
                    <th class="py-2 ">Actions</th>
                </tr>
            </thead>
            <tbody class="text-gray-600 text-sm font-light">
                @if ( count($orders) > 0 )
                    @foreach ($orders as $order)
                    
                        <tr class="border-b border-gray-200 hover:bg-gray-100">
                            
                            <td class="py-3 px-6 text-left whitespace-nowrap">
                                <div class="flex items-center">
                                    <div class="mr-2">                                        
                                    </div>
                                    <span class="font-medium">React Project</span>
                                </div>
                            </td>
    
                            <td class="py-3 px-6 text-left">
                                <div class="flex items-center ">
                                    <span>{{ $order->num_fact }}</span>
                                </div>
                            </td>
    
                            <td class="py-3 px-6 text-center hidden sm:table-cell">
                                <div class="flex items-center justify-center">
                                    {{ $order->delivery_day }}
                                </div>
                            </td>
    
                            <td class="py-3 px-6 text-center hidden sm:table-cell">
                                <div class="text-center">
                                    {{ $order->orderdetails->count() }}
                                </div>
                            </td>
    
                            <td class="py-3 px-6 text-center hidden md:table-cell">
                                @if ($order->estado)
                                <div class="text-white py-1 px-3 rounded-sm text-xs w-10/12 shadow-lg shadow-gray-500/30" style="background-color: {{ $order->estado->color }};">
                                    {{ $order->estado->name }}
                                </div>
                                @else
                                <div class="">
                                    Sin estado
                                </div>
                                @endif
                                
                            </td>
    
                            <td class="py-3 px-6 text-center hidden md:table-cell">
                                <div class="text-center">
                                    {{ $order->updated_at }}
                                </div>
                            </td>
    
                            <td class="py-3 px-6 text-center ">
                                <div class="flex item-center justify-center realtive" x-data="{abrir: false}">
                                    <div  class="flex gap-1">
                                        <span wire:click="listdetail({{ $order->id }})" class="bg-white cursor-pointer text-purple-500 border px-2 py-1 hover:shadow-lg rounded-md" x-on:click="abrir = !abrir" title="Detalle de la orden">
                                            <i class="fas fa-list-ul"></i>
                                        </span>
                                        <a href="{{ route('order',$order->id) }}" class="bg-white cursor-pointer text-gray-700 hover:text-teal-600 border px-2 py-1 hover:shadow-lg rounded-md " title="Kardex del producto"> 
                                            <i class="fas fa-cubes"></i>
                                        </a>
                                    </div>   
                                    
                                    <div class="border absolute rounded-md bg-white shadow-lg mt-8 right-0 sm:right-14 w-full sm:w-96 pb-1 overflow-hidden" x-show="abrir" x-on:click.away="abrir = false">
                                        <div class="py-1 shadow-sm font-bold flex px-3 justify-between drop-shadow-lg backdrop-opacity-20 backdrop-blur-md">
                                            <span class="">Detalle de la orden</span>
                                            <span x-on:click="abrir = false" class="bg-white rounded-md border px-2 hover:border-pink-400 hover:text-pink-400 cursor-pointer"><i class="fas fa-times"></i></span>
                                        </div>
                                        <div class="my-3 text-md" wire:loading.delay="listdetail({{ $order->id }})">cargando ...</div>
                                        @if (count($details)> 0)
                                            <ul wire:loading.attr="hidden"  class="px-2 text-left left-0 text-xs max-h-sm overflow-y-auto relative h-auto" style="max-height: 250px;">
                                                <div class="grid grid-cols-5 w-full py-1 hover:bg-gray-100 font-bold">
                                                    <li class="col-span-2">Items</li>
                                                    <li class="">Canty</li>
                                                    <li class="">Price</li>
                                                    <li class="">Total</li>
                                                </div>
                                                @foreach ($details as $detail)
                                                <div class="grid grid-cols-5 w-full py-1 hover:bg-gray-100">
                                                    <li class="col-span-2">{{ Str::limit($detail->product->name, 40, '...')  }}</li>
                                                    <li class="">{{ $detail->canty }}</li>
                                                    <li class="">{{ $detail->price }}</li>
                                                    <li class="">{{ $detail->canty*$detail->price }}</li>
                                                </div>
                                                    
                                                @endforeach
                                                
                                            </ul>
                                        @else
                                            <div class="px-4 py-2 w-full bg-gray-200 hover:bg-gray-100 font-bold my-2">
                                                No hay detalles
                                            </div>
                                        @endif
                                        
                                    </div>
                                </div>
                            </td>                            
                        </tr>
                        
                    
                    @endforeach
                @endif
                
    
            </tbody>
        </table>
    </div>
    

</div>
