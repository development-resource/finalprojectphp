<div>
    <div class="bg-white/10 backdrop-blur-xs py-5 px-4 relative" style="z-index: 10;">
        <ol class="items-center grid grid-flow-col bg-gray-50 p-1 rounded-md bg-white/30 backdrop-blur-md" style="z-index: 10;">
            
            <li class="relative mb-6 sm:mb-0" x-data="{open:false}" @mouseout="open = false"> <!-- x-on:mousedow.away="open= !open" @mouseup="open = false" @mousedown="open = false"-->
                <div class="flex items-center" @mouseover="open = true">
                    <div class="flex z-10 justify-center items-center w-5 h-5 bg-green-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0">
                        <i class="fas fa-check-circle fa-inverse"></i>
                    </div>
                    <div class="hidden sm:flex w-full bg-green-400 h-0.5 dark:bg-gray-700"></div>
                </div>
                <div class="z-10 mt-3 overflow-x-auto absolute w-auto bg-white shadow-lg border border-green-400 p-2 rounded-md mr-1 after:bg-green-400 after:h-5 after:w-5 after:rotate-45 after:absolute after:-top-3 after:rounded-sm" x-show="open">
                    <p class="text-sm font-semibold text-gray-700 dark:text-white pt-2">Venta registrada</p>
                    <time class="block mb-2 text-xs font-normal leading-none text-gray-600 dark:text-gray-500 pt-2"> <i class="far fa-clock"></i> 2022-05-18</time>
                    <p class="text-xs font-normal text-gray-600 dark:text-gray-400 overflow-hidden py-1"> <i class="fas fa-user"></i> Thonny Flores Thonny Flores Thonny Flores</p>
                </div>
            </li>
            <li class="relative mb-6 sm:mb-0" x-data="{open:false}" @mouseout="open = false">
                <div class="flex items-center" @mouseover="open = true">
                    <div class="flex z-10 justify-center items-center w-5 h-5 bg-green-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0">
                        <i class="fas fa-check-circle fa-inverse"></i>
                    </div>
                    <div class="hidden sm:flex w-full bg-green-400 h-0.5 dark:bg-gray-700"></div>
                </div>
                <div class="z-10 mt-3 overflow-x-auto absolute w-auto bg-white shadow-lg border border-green-400 p-2 rounded-md mr-1 after:bg-green-400 after:h-5 after:w-5 after:rotate-45 after:absolute after:-top-3 after:rounded-sm" x-show="open">
                    <p class="text-sm font-semibold text-gray-700 dark:text-white pt-2">Venta registrada</p>
                    <time class="block mb-2 text-xs font-normal leading-none text-gray-600 dark:text-gray-500 pt-2"> <i class="far fa-clock"></i> 2022-05-18</time>
                    <p class="text-xs font-normal text-gray-600 dark:text-gray-400 overflow-hidden py-1"> <i class="fas fa-user"></i> Thonny Flores Thonny Flores Thonny Flores</p>
                </div>
            </li>
            <li class="relative mb-6 sm:mb-0" x-data="{open:false}" @mouseout="open = false">
                <div class="flex items-center" @mouseover="open = true">
                    <div class="cursor-pointer group flex justify-center items-center w-5 h-5 bg-green-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0 relative">
                        <i class="fas fa-play-circle fa-inverse text-green-100 group-hover:scale-105 group-hover:duration-1000"></i>
                        <i class="hidden group-hover:block absolute fas fa-play-circle fa-inverse text-green-400 group-hover:scale-110 group-hover:duration-1000 group-hover:animate-ping"></i>
                    </div>
                    <div class="hidden sm:flex w-full bg-green-400 h-0.5 dark:bg-gray-700"></div>
                </div>
                <div class="z-10 mt-3 overflow-x-auto absolute w-auto bg-white shadow-lg border border-green-400 p-2 rounded-md mr-1 after:bg-green-400 after:h-5 after:w-5 after:rotate-45 after:absolute after:-top-3 after:rounded-sm" x-show="open">
                    <p class="text-sm font-semibold text-gray-700 dark:text-white pt-2">Venta registrada</p>
                    <time class="block mb-2 text-xs font-normal leading-none text-gray-600 dark:text-gray-500 pt-2"> <i class="far fa-clock"></i> 2022-05-18</time>
                    <p class="text-xs font-normal text-gray-600 dark:text-gray-400 overflow-hidden py-1"> <i class="fas fa-user"></i> Thonny Flores Thonny Flores Thonny Flores</p>
                </div>
            </li>
            <li class="relative mb-6 sm:mb-0" x-data="{open:false}" @mouseout="open = false">
                <div class="flex items-center" @mouseover="open = true">
                    <div class="flex z-10 justify-center items-center w-5 h-5 bg-gray-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0">
                        <i class="fas fa-pause-circle fa-inverse"></i>
                    </div>
                    <div class="hidden sm:flex w-full bg-gray-400 h-0.5 dark:bg-gray-700"></div>
                </div>
                <div class="z-10 mt-3 overflow-x-auto absolute w-auto bg-white shadow-lg border border-gray-400 p-2 rounded-md mr-1 after:bg-gray-400 after:h-5 after:w-5 after:rotate-45 after:absolute after:-top-3 after:rounded-sm" x-show="open">
                    <p class="text-sm font-semibold text-gray-700 dark:text-white pt-2">Venta registrada</p>
                    <time class="block mb-2 text-xs font-normal leading-none text-gray-600 dark:text-gray-500 pt-2"> <i class="far fa-clock"></i> 2022-05-18</time>
                    <p class="text-xs font-normal text-gray-600 dark:text-gray-400 overflow-hidden py-1"> <i class="fas fa-user"></i> Thonny Flores Thonny Flores Thonny Flores</p>
                </div>
            </li>
            <li class="relative mb-6 sm:mb-0" x-data="{open:false}" @mouseout="open = false">
                <div class="flex items-center" @mouseover="open = true">
                    <div class="flex justify-center items-center w-5 h-5 bg-gray-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0">
                        <i class="fas fa-pause-circle fa-inverse"></i>
                    </div>
                    <div class="hidden sm:flex w-full bg-gray-400 h-0.5 dark:bg-gray-700"></div>
                </div>
                <div class="z-10 mt-3 overflow-x-auto absolute w-auto bg-white shadow-lg border border-gray-400 p-2 rounded-md mr-1 after:bg-gray-400 after:h-5 after:w-5 after:rotate-45 after:absolute after:-top-3 after:rounded-sm" x-show="open">
                    <p class="text-sm font-semibold text-gray-700 dark:text-white pt-2">Venta registrada</p>
                    <time class="block mb-2 text-xs font-normal leading-none text-gray-600 dark:text-gray-500 pt-2"> <i class="far fa-clock"></i> 2022-05-18</time>
                    <p class="text-xs font-normal text-gray-600 dark:text-gray-400 overflow-hidden py-1"> <i class="fas fa-user"></i> Thonny Flores Thonny Flores Thonny Flores</p>
                </div>
                <div class="absolute bottom-0 top-0 right-0 w-5 h-5 bg-gray-400 rounded-full ring-0 ring-black dark:bg-blue-900  dark:ring-gray-900 shrink-0">
                    <i class="fas fa-pause-circle fa-inverse"></i>
                </div>
            </li>   
                  
        </ol>
    </div>

    <div class=" md:bg-white mt-2 rounded-md shadow-md border ">
        <div class="bg-gradient-to-br from-slate-100 to-white  relative " style="z-index: 1;">
            {{-- <div class="relative col-start-1 row-start-1 w-full flex-col justify-between gap-10 border px-5 bg-transparent"> --}}
            <div class="relative w-full md:flex px-5 bg-transparent py-5">
                
                <div class="px-3 grid md:flex gap-4 justify-between w-full">
                    
                    <div class="w-full max-h-96 md:w-60 md:max-w-96 bg-gray-600/80 md:bg-gray-200/50 md:backdrop-blur-xl rounded-lg border border-gray-50 shadow-md  dark:bg-gray-800 dark:border-gray-700 overflow-hidden">
                        <div class=" mb-4 px-4 py-2 shadow-sm bg-white/40 backdrop-blur-xl  rounded-t-md">
                            <h3 class="text-sm font-bold text-white dark:text-white uppercase text-center flex my-auto w-full justify-center">
                                Pedido
                            </h3>                            
                        </div>

                        <div class="flow-root ">
                            <ul class="divide-y  dark:divide-gray-700 overflow-y-auto max-h-72 " role="list">
                                @if ($order)
                                    
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2">
                                        <div class="flex items-center space-x-4 group">                                        
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Numero de orden
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->num_fact }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Fecha de entrega
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->delivery_day }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Estado Actual
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->status }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Código de guía
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->codigo_guia }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Fecha reg de guía
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->codigo_guia }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Cliente
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->empr_nroruc }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    Fecha de creación
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->created_at }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="py-3 sm:py-2 hover:bg-gray-200 px-2 group">
                                        <div class="flex items-center space-x-4">
                                            <div class="flex-1 min-w-0">
                                                <p class="text-sm font-extrabold text-gray-900 truncate dark:text-white">
                                                    F. última actualización
                                                </p>
                                                <p class="text-sm text-gray-200 group-hover:text-gray-800 truncate dark:text-gray-400">
                                                    {{ $order->updated_at }}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                
                                @endif
                            </ul>
                        </div>
                        <div class="px-3 text-xs text-gray-600 pt-2">
                            Total Items:  <span class="bg-orange-500 text-white font-extrabold rounded-full px-1">{{ count($orderdetails) }}</span> <br>
                            F. Act:  {{ $order->updated_at }} 
                        </div>
                    </div>
                    
                    <div class="w-full border text-gray-900 border-gray-200 rounded-md bg-gray-300/30 backdrop-blur-md overflow-x-hidden">
                        
                        <div class="shadow-md rounded  overflow-x-auto max-h-96 w-full">
                            <table class=" w-full table-auto">
                                <thead>
                                    <tr class="top-0 sticky bg-white/40 backdrop-blur-xl text-white uppercase text-xs sm:text-md font-extrabold">
                                        <th class="top-0 sticky py-3 px-6 text-left">
                                            Items
                                        </th>
                                        <th class="top-0 sticky py-3 px-6 text-left">
                                            Nombre
                                        </th>
                                        <th class="top-0 sticky py-3 px-6 text-center">
                                            Cantidad
                                        </th>
                                        <th class="top-0 sticky py-3 px-6 text-center">
                                            Precio
                                        </th>
                                        <th class="top-0 sticky py-3 px-6 text-center">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
                                <tbody class="text-black text-xs font-light sm:text-sm">
                                    @if (count($orderdetails)> 0)
                                        @foreach ($orderdetails as $orderdetail)
                                            <tr class="border-b border-gray-200 hover:bg-gray-100">
                                                <td class="py-3 px-6 text-left whitespace-nowrap">
                                                    <div class="flex items-center">                                                        
                                                        {{ $loop->index+1 }}
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-left">
                                                    <div class="flex items-center">                                                        
                                                        {{ $orderdetail->product_name }}
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-center">
                                                    <div class="bg-white text-teal-500 py-1 px-3 rounded-full text-sm font-extrabold w-full">
                                                        {{ $orderdetail->canty }}
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-center">
                                                    <div class="flex items-center justify-center">
                                                        {{ $orderdetail->price }}
                                                    </div>
                                                </td>
                                                <td class="py-3 px-6 text-center">
                                                    <div class="flex item-center justify-center">
                                                        {{ $orderdetail->created_at }}
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    
                                </tbody>
                            </table>
                        </div>
                                                
                    </div>
                    
                </div>
                
            </div>
            
            <svg class="col-start-1 row-start-1 h-auto w-full absolute bg-black"
                style="margin:auto; z-index:-1; position:absolute; bottom: 0px; background-color: rgba(255, 255, 255, 0.9);"
                height="850" width="1554" preserveaspectratio="xMidYMid"  viewbox="0 0 1554 847"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <g transform="translate(777,423.5) scale(1,1) translate(-777,-423.5)">
                    <lineargradient id="lg-0.26746606666069006" x1="0" x2="1" y1="0" y2="0">
                        <stop offset="0" stop-color="#1e3a8a">
                        </stop>
                        <stop offset="1" stop-color="#00ffff">
                        </stop>
                    </lineargradient>
                    <path d="" fill="url(#lg-0.26746606666069006)" opacity="0.4">
                        <animate attributename="d" begin="0s" calcmode="spline" dur="10s" keysplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" keytimes="0;0.333;0.667;1" repeatcount="indefinite" values="M0 0L 0 760.7026214267527Q 155.4 785.1233671175083  310.8 750.2229361955833T 621.6 603.8697346171566T 932.4 561.0739803269321T 1243.2 413.6751202183865T 1554 263.70723909036207L 1554 0 Z;M0 0L 0 718.8886662590857Q 155.4 736.543586493918  310.8 694.7160557852296T 621.6 654.1037984564883T 932.4 574.7037841784775T 1243.2 427.9005213584073T 1554 414.02677086228044L 1554 0 Z;M0 0L 0 816.4233228257277Q 155.4 717.5532199331701  310.8 694.9234849695399T 621.6 605.7174706688634T 932.4 535.291044128593T 1243.2 472.1982875395818T 1554 298.19317271285473L 1554 0 Z;M0 0L 0 760.7026214267527Q 155.4 785.1233671175083  310.8 750.2229361955833T 621.6 603.8697346171566T 932.4 561.0739803269321T 1243.2 413.6751202183865T 1554 263.70723909036207L 1554 0 Z">
                        </animate>
                    </path>
                    <path d="" fill="url(#lg-0.26746606666069006)" opacity="0.4">
                        <animate attributename="d" begin="-2s" calcmode="spline" dur="10s" keysplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" keytimes="0;0.333;0.667;1" repeatcount="indefinite" values="M0 0L 0 794.2365750032683Q 155.4 678.2607576369991  310.8 661.0398185389812T 621.6 664.1529097167671T 932.4 473.1248153404357T 1243.2 458.0647652680569T 1554 356.5868647876463L 1554 0 Z;M0 0L 0 845.782995795947Q 155.4 714.1055858541522  310.8 688.2796572037822T 621.6 665.6961973832547T 932.4 486.8902236275524T 1243.2 367.5274880573601T 1554 350.32012734006076L 1554 0 Z;M0 0L 0 808.1737972335859Q 155.4 768.5199464234902  310.8 745.4525319979058T 621.6 587.0467045897211T 932.4 496.079189234979T 1243.2 467.93318903379065T 1554 381.90818535649396L 1554 0 Z;M0 0L 0 794.2365750032683Q 155.4 678.2607576369991  310.8 661.0398185389812T 621.6 664.1529097167671T 932.4 473.1248153404357T 1243.2 458.0647652680569T 1554 356.5868647876463L 1554 0 Z">
                        </animate>
                    </path>
                    <path d="" fill="url(#lg-0.26746606666069006)" opacity="0.4">
                        <animate attributename="d" begin="-4s" calcmode="spline" dur="10s" keysplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" keytimes="0;0.333;0.667;1" repeatcount="indefinite" values="M0 0L 0 725.6084375507925Q 155.4 685.2746659312883  310.8 669.0371440944939T 621.6 573.2665682053512T 932.4 469.0750743377805T 1243.2 407.12114448438393T 1554 455.3095205314213L 1554 0 Z;M0 0L 0 831.645547560626Q 155.4 749.7146386559377  310.8 721.5988963264405T 621.6 673.1772653623518T 932.4 540.7216238974822T 1243.2 542.1673213058692T 1554 352.8635909405122L 1554 0 Z;M0 0L 0 715.4988907428713Q 155.4 774.3901654774675  310.8 732.1739357372687T 621.6 582.8041865207543T 932.4 545.330814778252T 1243.2 421.3443988798452T 1554 327.01169756198624L 1554 0 Z;M0 0L 0 725.6084375507925Q 155.4 685.2746659312883  310.8 669.0371440944939T 621.6 573.2665682053512T 932.4 469.0750743377805T 1243.2 407.12114448438393T 1554 455.3095205314213L 1554 0 Z">
                        </animate>
                    </path>
                    <path d="" fill="url(#lg-0.26746606666069006)" opacity="0.9">
                        <animate attributename="d" begin="-6s" calcmode="spline" dur="8s" keysplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" keytimes="0;0.333;0.667;1" repeatcount="indefinite" values="M0 0L 0 730.5601327199832Q 155.4 759.2045177759919  310.8 722.3167097190079T 621.6 618.3364834094375T 932.4 476.524327777702T 1243.2 398.2771261349009T 1554 355.2930031546914L 1554 0 Z;M0 0L 0 724.9696889579322Q 155.4 683.0193681825691  310.8 655.6687705987517T 621.6 676.5335933397497T 932.4 444.34760370791156T 1243.2 476.3655305218616T 1554 413.7781302883305L 1554 0 Z;M0 0L 0 713.4670206106769Q 155.4 783.7265216257797  310.8 739.7847743615473T 621.6 574.4623566500566T 932.4 503.0945464216904T 1243.2 455.1550816826908T 1554 306.94902038573514L 1554 0 Z;M0 0L 0 730.5601327199832Q 155.4 759.2045177759919  310.8 722.3167097190079T 621.6 618.3364834094375T 932.4 476.524327777702T 1243.2 398.2771261349009T 1554 355.2930031546914L 1554 0 Z">
                        </animate>
                    </path>
                    <path d="" fill="url(#lg-0.26746606666069006)" opacity="0.4">
                        <animate attributename="d" begin="-8s" calcmode="spline" dur="10s" keysplines="0.5 0 0.5 1;0.5 0 0.5 1;0.5 0 0.5 1" keytimes="0;0.333;0.667;1" repeatcount="indefinite" values="M0 0L 0 750.5067464298497Q 155.4 799.0798684522905  310.8 757.6576146327097T 621.6 682.4225263879337T 932.4 542.2516559700182T 1243.2 476.6613733791618T 1554 326.5540011037318L 1554 0 Z;M0 0L 0 724.894058224438Q 155.4 728.7221393584286  310.8 703.5384321946935T 621.6 592.8194707609489T 932.4 463.55839545127805T 1243.2 510.5012368313626T 1554 425.23696974018395L 1554 0 Z;M0 0L 0 817.2484162199289Q 155.4 709.0371644738543  310.8 667.1171234517955T 621.6 581.805912543598T 932.4 585.0043982331314T 1243.2 406.46883011885933T 1554 446.7347421398804L 1554 0 Z;M0 0L 0 750.5067464298497Q 155.4 799.0798684522905  310.8 757.6576146327097T 621.6 682.4225263879337T 932.4 542.2516559700182T 1243.2 476.6613733791618T 1554 326.5540011037318L 1554 0 Z">
                        </animate>
                    </path>
                </g>
            </svg>
            
        </div>

        
        
    </div>
    <div class="bg-white mt-2 rounded-md shadow-md border">
        <div class="bg-gray-100 items-center flex mx-auto border py-3 px-4 w-full">
            <div class=" bg-white border text-green-700 rounded-md w-full" style="z-index: 2;">
                <div class="px-2 pt-2">
                    <div class="flex justify-between space-x-1">
                        <div class="mx-2 flex-1 justify-center px-2 md:flex md:justify-start">
                            <span class="text-2xl font-bold">
                                Lista de ordenes del último mes
                            </span>
                        </div>
                    </div>
                </div>
                <div class="w-full relative">
                    <livewire:listorders />
                </div>
            </div>
            
        </div>
    </div>
        
    <div class="w-full mt-14 bg-gray-800 text-gray-200 rounded-lg py-5 px-4">
        <div class="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-4 md:gap-8 gap-4">
            <div class="flex flex-col flex-shrink-0">
                <div class="dark:text-white">
                    <img class="dark:hidden" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/footer-I-svg1.svg" alt="icon">
                    <img class="hidden dark:block" src="https://tuk-cdn.s3.amazonaws.com/can-uploader/footer-I-svg1dark.svg" alt="icon">
                </div>
                <p class="text-sm leading-none mt-4 dark:text-white">Copyright © {{  date('Y') }} Corporation DEV</p>
                <p class="text-sm leading-none mt-4 dark:text-white">Todo los derechos reservados</p>
                <div class="flex items-center gap-x-4 mt-12">
                    <button aria-label="instagram" class="border focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 opacity-50 w-8 h-8 flex-shrink-0 bg-gray-800 cursor-pointer hover:bg-gray-700 rounded-full flex items-center justify-center">
                        <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M9.00081 0.233398C6.68327 0.233398 6.39243 0.243215 5.48219 0.283343C4.57374 0.323644 3.95364 0.462973 3.41106 0.667403C2.84981 0.87855 2.37372 1.161 1.8994 1.62066C1.42473 2.08016 1.13317 2.54137 0.914502 3.08491C0.702944 3.61071 0.558942 4.2116 0.518053 5.09132C0.477342 5.97311 0.466675 6.25504 0.466675 8.50015C0.466675 10.7453 0.476986 11.0262 0.518231 11.9079C0.560009 12.788 0.703833 13.3887 0.914679 13.9144C1.13282 14.4581 1.42437 14.9193 1.89887 15.3788C2.37301 15.8386 2.8491 16.1218 3.40999 16.3329C3.95293 16.5373 4.57321 16.6767 5.48148 16.717C6.39171 16.7571 6.68238 16.7669 8.99974 16.7669C11.3175 16.7669 11.6074 16.7571 12.5176 16.717C13.4261 16.6767 14.0469 16.5373 14.5898 16.3329C15.1509 16.1218 15.6263 15.8386 16.1004 15.3788C16.5751 14.9193 16.8667 14.4581 17.0853 13.9145C17.2951 13.3887 17.4391 12.7878 17.4818 11.9081C17.5227 11.0263 17.5333 10.7453 17.5333 8.50015C17.5333 6.25504 17.5227 5.97328 17.4818 5.09149C17.4391 4.21143 17.2951 3.61071 17.0853 3.08508C16.8667 2.54137 16.5751 2.08016 16.1004 1.62066C15.6258 1.16082 15.1511 0.878377 14.5893 0.667403C14.0453 0.462973 13.4249 0.323644 12.5164 0.283343C11.6062 0.243215 11.3164 0.233398 8.99814 0.233398H9.00081ZM8.23525 1.72311C8.46245 1.72277 8.71597 1.72311 9.00077 1.72311C11.2792 1.72311 11.5492 1.73104 12.449 1.77065C13.281 1.8075 13.7326 1.94218 14.0334 2.05533C14.4316 2.20517 14.7155 2.38428 15.014 2.67362C15.3127 2.96295 15.4976 3.23851 15.6526 3.62429C15.7694 3.91535 15.9086 4.3528 15.9464 5.15881C15.9873 6.03026 15.9962 6.29204 15.9962 8.49823C15.9962 10.7044 15.9873 10.9662 15.9464 11.8377C15.9084 12.6437 15.7694 13.0811 15.6526 13.3722C15.4979 13.758 15.3127 14.0327 15.014 14.3218C14.7153 14.6112 14.4318 14.7903 14.0334 14.9401C13.7329 15.0538 13.281 15.1881 12.449 15.225C11.5494 15.2646 11.2792 15.2732 9.00077 15.2732C6.72217 15.2732 6.45212 15.2646 5.55256 15.225C4.72055 15.1878 4.26899 15.0531 3.96801 14.9399C3.56978 14.7901 3.28533 14.611 2.98666 14.3216C2.68799 14.0323 2.5031 13.7574 2.34808 13.3715C2.23128 13.0804 2.09208 12.643 2.05421 11.837C2.01332 10.9655 2.00514 10.7037 2.00514 8.49617C2.00514 6.2886 2.01332 6.0282 2.05421 5.15674C2.09226 4.35073 2.23128 3.91329 2.34808 3.62188C2.50275 3.2361 2.68799 2.96054 2.98666 2.67121C3.28533 2.38187 3.56978 2.20276 3.96801 2.05258C4.26881 1.93891 4.72055 1.80457 5.55256 1.76755C6.33977 1.7331 6.64484 1.72277 8.23525 1.72105V1.72311ZM13.5558 3.09574C12.9905 3.09574 12.5318 3.53956 12.5318 4.08741C12.5318 4.63508 12.9905 5.07942 13.5558 5.07942C14.1212 5.07942 14.5799 4.63508 14.5799 4.08741C14.5799 3.53974 14.1212 3.09574 13.5558 3.09574ZM9.00082 4.25481C6.58071 4.25481 4.61855 6.15564 4.61855 8.50013C4.61855 10.8446 6.58071 12.7446 9.00082 12.7446C11.4209 12.7446 13.3824 10.8446 13.3824 8.50013C13.3824 6.15564 11.4209 4.25481 9.00082 4.25481ZM9.00079 5.74454C10.5717 5.74454 11.8453 6.97818 11.8453 8.50013C11.8453 10.0219 10.5717 11.2557 9.00079 11.2557C7.42975 11.2557 6.15632 10.0219 6.15632 8.50013C6.15632 6.97818 7.42975 5.74454 9.00079 5.74454Z"
                                fill="white"
                            />
                        </svg>
                    </button>
                    <button aria-label="linked-in" class="border focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 opacity-50 w-8 h-8 flex-shrink-0 bg-gray-800 cursor-pointer hover:bg-gray-700 rounded-full flex items-center justify-center">
                        <svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M17.5333 8.4886C17.5333 9.04766 17.4746 9.60594 17.3592 10.1501C17.2467 10.6814 17.08 11.203 16.8617 11.7016C16.6483 12.1914 16.3837 12.6634 16.0745 13.1035C15.77 13.5409 15.4191 13.9512 15.0337 14.3253C14.6474 14.6977 14.2224 15.0367 13.7711 15.333C13.3152 15.6304 12.8273 15.8864 12.3215 16.094C11.806 16.3044 11.2664 16.4657 10.7184 16.5745C10.1559 16.6866 9.57755 16.7438 8.99962 16.7438C8.42126 16.7438 7.8429 16.6866 7.28121 16.5745C6.73244 16.4657 6.19283 16.3044 5.67779 16.094C5.17195 15.8864 4.68357 15.6304 4.22772 15.333C3.77645 15.0367 3.35143 14.6977 2.96599 14.3253C2.58015 13.9512 2.22928 13.5409 1.92427 13.1035C1.61675 12.6634 1.35172 12.1913 1.13755 11.7016C0.919183 11.203 0.752114 10.6814 0.639188 10.1501C0.525025 9.60594 0.466675 9.04766 0.466675 8.4886C0.466675 7.92913 0.524992 7.36965 0.639221 6.82665C0.752147 6.29538 0.919216 5.77299 1.13759 5.27519C1.35175 4.78505 1.61678 4.31265 1.9243 3.87246C2.22931 3.43473 2.58018 3.02517 2.96602 2.65069C3.35146 2.27823 3.77648 1.94007 4.22775 1.64421C4.6836 1.3455 5.17198 1.08958 5.67783 0.881567C6.19286 0.670713 6.73244 0.509099 7.28124 0.401087C7.84294 0.289844 8.4213 0.233398 8.99966 0.233398C9.57758 0.233398 10.1559 0.289844 10.7185 0.401087C11.2664 0.509131 11.806 0.670745 12.3215 0.881567C12.8273 1.08955 13.3153 1.3455 13.7711 1.64421C14.2224 1.94007 14.6475 2.27823 15.0337 2.65069C15.4191 3.02517 15.77 3.43473 16.0746 3.87246C16.3837 4.31265 16.6483 4.78508 16.8617 5.27519C17.08 5.77299 17.2467 6.29538 17.3592 6.82665C17.4746 7.36965 17.5333 7.92913 17.5333 8.4886ZM5.89026 2.11217C3.85805 3.0405 2.34131 4.85195 1.86836 7.03507C2.06048 7.03668 5.0973 7.07377 8.59622 6.17446C7.33492 4.00666 5.98735 2.23757 5.89026 2.11217ZM9.2 7.26001C5.44774 8.34669 1.84711 8.2685 1.71795 8.26369C1.71585 8.33945 1.71211 8.4128 1.71211 8.4886C1.71211 10.2996 2.41839 11.9507 3.57929 13.1991C3.57678 13.1954 5.57108 9.77282 9.50377 8.54262C9.59876 8.51199 9.69546 8.48456 9.79128 8.45797C9.60838 8.05732 9.40875 7.65584 9.2 7.26001ZM13.8124 3.1977C12.5293 2.10329 10.8447 1.43946 8.9996 1.43946C8.40748 1.43946 7.83286 1.50879 7.28242 1.63697C7.39157 1.77887 8.76042 3.53549 10.0067 5.74921C12.7565 4.75199 13.7944 3.22348 13.8124 3.1977ZM10.288 9.6261C10.2718 9.63131 10.2556 9.6358 10.2397 9.64142C5.93997 11.0914 4.53583 14.0136 4.52064 14.0455C5.75781 14.9762 7.30956 15.5377 8.99965 15.5377C10.0088 15.5377 10.9701 15.339 11.8448 14.9791C11.7368 14.3632 11.3135 12.2042 10.288 9.6261ZM13.0719 14.3349C14.7082 13.2668 15.8703 11.5706 16.1945 9.60591C16.0445 9.55916 14.0057 8.93477 11.6535 9.29958C12.6093 11.8407 12.9977 13.9101 13.0719 14.3349ZM10.5676 6.79966C10.7368 7.13585 10.9006 7.47801 11.0518 7.82188C11.1056 7.94524 11.1581 8.06618 11.2093 8.18708C13.7128 7.88233 16.1792 8.39506 16.2846 8.41599C16.2679 6.74483 15.65 5.21108 14.6275 4.01032C14.6137 4.02922 13.4449 5.66294 10.5676 6.79966Z"
                                fill="white"
                            />
                        </svg>
                    </button>
                    <button aria-label="twitter" class="border focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 opacity-50 w-8 h-8 flex-shrink-0 bg-gray-800 cursor-pointer hover:bg-gray-700 rounded-full flex items-center justify-center">
                        <svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M7.5208 3.59864L7.55438 4.13498L6.99479 4.0693C4.95791 3.81755 3.17843 2.9638 1.66756 1.52992L0.928908 0.818458L0.73865 1.34385C0.33575 2.51503 0.593158 3.75188 1.43253 4.58375C1.8802 5.04346 1.77948 5.10914 1.00725 4.8355C0.73865 4.74793 0.503625 4.68226 0.481242 4.71509C0.4029 4.79171 0.6715 5.78776 0.884142 6.18181C1.17513 6.72909 1.76828 7.26542 2.4174 7.58284L2.96579 7.83459L2.31668 7.84554C1.68994 7.84554 1.66756 7.85648 1.73471 8.08634C1.95854 8.79781 2.84268 9.55305 3.82755 9.88142L4.52143 10.1113L3.91708 10.4615C3.02175 10.965 1.96973 11.2496 0.917717 11.2715C0.414092 11.2825 0 11.3262 0 11.3591C0 11.4685 1.36538 12.0815 2.15999 12.3223C4.54382 13.0338 7.37531 12.7273 9.50173 11.5123C11.0126 10.6476 12.5235 8.92915 13.2286 7.26542C13.6091 6.37883 13.9896 4.75888 13.9896 3.98174C13.9896 3.47824 14.0232 3.41257 14.6499 2.81056C15.0192 2.4603 15.3662 2.0772 15.4333 1.96775C15.5452 1.75978 15.534 1.75978 14.9633 1.94586C14.012 2.27422 13.8777 2.23044 14.3477 1.73789C14.6947 1.38763 15.1088 0.752784 15.1088 0.566709C15.1088 0.533872 14.9409 0.5886 14.7506 0.68711C14.5492 0.796566 14.1015 0.96075 13.7658 1.05926L13.1614 1.24534L12.613 0.884131C12.3108 0.68711 11.8856 0.468198 11.6617 0.402524C11.0909 0.249286 10.218 0.271177 9.70318 0.446307C8.30422 0.938859 7.42008 2.20855 7.5208 3.59864Z"
                                fill="white"
                            />
                        </svg>
                    </button>
                    <button aria-label="youtube" class="border focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 opacity-50 w-8 h-8 flex-shrink-0 bg-gray-800 cursor-pointer hover:bg-gray-700 rounded-full flex items-center justify-center">
                        <svg  width="18" height="13" viewBox="0 0 18 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                clip-rule="evenodd"
                                d="M15.6677 1.17143C16.4021 1.36664 16.9804 1.94183 17.1767 2.67227C17.5333 3.99611 17.5333 6.75832 17.5333 6.75832C17.5333 6.75832 17.5333 9.52043 17.1767 10.8444C16.9804 11.5748 16.4021 12.15 15.6677 12.3453C14.3369 12.7 9.00001 12.7 9.00001 12.7C9.00001 12.7 3.66309 12.7 2.33218 12.3453C1.59783 12.15 1.0195 11.5748 0.823232 10.8444C0.466675 9.52043 0.466675 6.75832 0.466675 6.75832C0.466675 6.75832 0.466675 3.99611 0.823232 2.67227C1.0195 1.94183 1.59783 1.36664 2.33218 1.17143C3.66309 0.81665 9.00001 0.81665 9.00001 0.81665C9.00001 0.81665 14.3369 0.81665 15.6677 1.17143ZM7.40002 4.43326V9.59993L11.6667 7.01669L7.40002 4.43326Z"
                                fill="white"
                            />
                        </svg>
                    </button>
                </div>
            </div>
            <div class="sm:ml-0 ml-8 flex flex-col">
                <h2 class="text-base font-semibold leading-4 dark:text-white">Company</h2>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Blog</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Pricing</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">About Us</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Contact us</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Testimonials</a>
            </div>
            <div class="sm:ml-0 ml-8 flex flex-col">
                <h2 class="text-base font-semibold leading-4 dark:text-white">Support</h2>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Legal policy</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Status policy</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Privacy policy</a>
                <a class="focus:outline-none focus:underline hover:text-gray-500 text-base leading-4 mt-6 dark:text-white cursor-pointer">Terms of service</a>
            </div>
            <div class="mt-10 lg:block hidden">
                <label class="text-xl font-medium leading-5 dark:text-white">Quiero software completo</label>
                <div class="flex items-center justify-between border border-gray-800 dark:border-white mt-4">
                    <a  class="bg-gray-300 text-gray-900 py-2 px-4 focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 cursor-pointer">
                        Ir a Home
                    </a>
                </div>
            </div>
        </div>
    </div>
    
    
   
</div>
