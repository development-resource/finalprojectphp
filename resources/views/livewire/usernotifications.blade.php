<div>
    <div class="rounded-t mb-0 px-0 border-0 bg-white">
        <div class="flex flex-wrap items-center px-4 py-2">
            <div class="relative w-full max-w-full flex-grow flex-1">
                <h3 class="font-semibold text-base text-gray-900 dark:text-gray-50 ">Tiene <span class="bg-orange-500 text-white text-sm rounded-md px-1 text-center justify-center">{{ count($notifications) }} </span> notificaciones</h3>
            </div>
            <div class="relative w-full max-w-full flex-grow flex-1 text-right">
                <button class="bg-blue-500 dark:bg-gray-100 text-white active:bg-blue-600 dark:text-gray-800 dark:active:text-gray-700 text-xs font-bold uppercase px-3 py-1 rounded outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150" type="button">
                    Ver todo
                </button>
            </div>
        </div>
        <div class="block w-full">
            <div class="px-4 bg-gray-200 mx-1 text-gray-700 dark:bg-gray-600  dark:text-gray-100 align-middle border border-solid border-gray-200 dark:border-gray-500 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                Hoy día
            </div>

            @if (count($notifications)>0)
                <ul class="my-1">
                    @foreach ($notifications as $notification)
                        @if ($notification->pivot->created_at>=$today)
                            <li class="flex px-4">
                                <div class="w-5 h-5 rounded-full flex-shrink-0 bg-indigo-500 my-2 mr-3">
                                    <svg class="w-5 h-5 fill-current text-indigo-50" viewBox="0 0 36 36"><path d="M18 10c-4.4 0-8 3.1-8 7s3.6 7 8 7h.6l5.4 2v-4.4c1.2-1.2 2-2.8 2-4.6 0-3.9-3.6-7-8-7zm4 10.8v2.3L18.9 22H18c-3.3 0-6-2.2-6-5s2.7-5 6-5 6 2.2 6 5c0 2.2-2 3.8-2 3.8z"></path></svg>
                                </div>
                                <div class="flex-grow flex items-center border-b hover:bg-gray-200 border-gray-100 dark:border-gray-400 text-sm text-gray-600 dark:text-gray-100 py-2">
                                    <div class="flex-grow flex justify-between items-center" x-data="{ open:false }">
                                        <div class="self-center">
                                            <div class="text-xs">
                                                <span class="font-medium italic text-gray-800 dark:text-gray-50 dark:hover:text-gray-100 " href="#0">
                                                    {{ $notification->title }}
                                                </span>
                                                <span class="font-bold text-gray-700" x-show="open">
                                                    {{ $notification->message }}
                                                </span>
                                            </div>
                                            <span class="text-xs" x-show="open"> {{ $notification->pivot->created_at }}</span>
                                        </div>
                                        <div class="ml-2 gap-2 text-center ">
                                            <span x-on:click="open = ! open" class="py-1 text-center cursor-pointer border rounded-md flex mx-auto bg-white items-center font-medium text-blue-500 hover:text-indigo-600 px-2">
                                                <i class="fas fa-eye mx-auto" x-show="!open" title="Ver detalles"></i>
                                                <i class="fas fa-eye-slash mx-auto" x-show="open" title="Ocultar"></i>
                                            </span>
                                            <span x-show="open" wire:click="readmessage({{ $notification->pivot->id }})" class="py-1 mx-auto text-center cursor-pointer border rounded-md flex text-green-500 bg-white items-center font-medium hover:text-green-600 text-blue-6 dark:text-blue-500  px-2">
                                                <i class="fas fa-check-circle mx-auto" title="Marcar como leido"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            @else
                <div class="bg-white text-gray-700 px-3">
                    No tiene notificaciones
                </div>
            @endif

            <div class="px-4 bg-gray-200 mx-1 text-gray-700 dark:bg-gray-600 dark:text-gray-100 align-middle border border-solid border-gray-200 dark:border-gray-500 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                Dias Atras
            </div>
            @if (count($notifications)>0)
                <ul class="my-1">
                    @foreach ($notifications as $notification)
                        @if ($notification->pivot->created_at<$today)
                        <li class="flex px-4">
                            <div class="w-5 h-5 rounded-full flex-shrink-0 bg-indigo-500 my-2 mr-3">
                                <svg class="w-5 h-5 fill-current text-indigo-50" viewBox="0 0 36 36"><path d="M18 10c-4.4 0-8 3.1-8 7s3.6 7 8 7h.6l5.4 2v-4.4c1.2-1.2 2-2.8 2-4.6 0-3.9-3.6-7-8-7zm4 10.8v2.3L18.9 22H18c-3.3 0-6-2.2-6-5s2.7-5 6-5 6 2.2 6 5c0 2.2-2 3.8-2 3.8z"></path></svg>
                            </div>
                            <div class="flex-grow flex items-center border-b hover:bg-gray-200 border-gray-100 dark:border-gray-400 text-sm text-gray-600 dark:text-gray-100 py-2">
                                <div class="flex-grow flex justify-between items-center" x-data="{ open:false }">
                                    <div class="self-center">
                                        <div class="text-xs">
                                            <span class="font-medium italic text-gray-800 dark:text-gray-50 dark:hover:text-gray-100 " href="#0">
                                                {{ $notification->title }}
                                            </span>
                                            <span class="font-bold text-gray-700" x-show="open">
                                                {{ $notification->message }}
                                            </span>
                                        </div>
                                        <span class="text-xs" x-show="open"> {{ $notification->pivot->created_at }}</span>
                                    </div>
                                    <div class="ml-2 gap-2 text-center ">
                                        <span x-on:click="open = ! open" class="py-1 text-center cursor-pointer border rounded-md flex mx-auto bg-white items-center font-medium text-blue-500 hover:text-indigo-600 px-2">
                                            <i class="fas fa-eye mx-auto" x-show="!open" title="Ver detalles"></i>
                                            <i class="fas fa-eye-slash mx-auto" x-show="open" title="Ocultar"></i>
                                        </span>
                                        <span x-show="open" wire:click="readmessage({{ $notification->pivot->id }})" class="py-1 mx-auto text-center cursor-pointer border rounded-md flex text-green-500 bg-white items-center font-medium hover:text-green-600 text-blue-6 dark:text-blue-500  px-2">
                                            <i class="fas fa-check-circle mx-auto" title="Marcar como leido"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        @endif
                        
                    @endforeach
                </ul>
            @else
                <div class="bg-white text-gray-700 px-3">
                    No tiene notificaciones pasadas
                </div>
            @endif
            
        </div>
    </div>
</div>
