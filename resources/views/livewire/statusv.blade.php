<div class="relative">
    <div class="px-3 overflow-y-auto overflow-x-hidden h-full relative" style="max-height: calc(100vh - 16.8rem); min-height:calc(100vh - 16.8rem); " >
        {{-- addStatus --}}
        @if ($addStatus_id==$operation_id)
            <div class="absolute border-2 border-white bg-gray-200/30 backdrop-blur-md shadow-2xl mx-2 px-3 py-2 w-full rounded-xl -ml-3 mb-4" style="margin-bottom: 10px; z-index: 8;">
                <form wire:submit.prevent="newStatu" class="w-full">
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Nombre estado
                            </label>
                            <input wire:model.defer="name" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Nombre de estado">
                            @error('name')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Código de estado
                            </label>
                            <input wire:model.defer="code" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Código de estado">
                            @error('code')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Orden
                            </label>
                            <input wire:model.defer="order" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="number" step="1" min="1" placeholder="Orden de estado">
                            @error('order')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Mensaje del evento
                            </label>
                            <input wire:model.defer="user_note" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Ingrese el mensaje">
                            @error('user_note')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Color
                            </label>
                            <input wire:model.defer="color" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Color">
                            @error('color')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Actualiza kardex?
                            </label>
                            <select wire:model.defer="io" class="w-full text-sm rounded-md border-gray-100 active:ring-0 focus:ring-0">                                            
                                <option class="py-1 px-2" value="-1">sin movimiento</option>
                                <option class="py-1 px-2" value="0">Ingreso a kardex <i class="ml-2 fas fa-check-circle text-green-500 font-bold"></i> </option>
                                <option  class="py-1 px-2" value="1">(-) Salida d kardex <i class="ml-2 fas fa-minus-circle text-pink-500 font-bold"></i> </option>
                            </select>   
                            @error('io')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 flex justify-between ">
                            <button type="sutmit" class="bg-green-500 hover:bg-green-600 shadow-lg px-3 py-1 rounded-md text-white">Agregar</button>
                            <button wire:click="canceAdd" type="reset" class="bg-pink-500 hover:bg-pink-600 shadow-lg px-3 py-1 rounded-md text-white">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        @endif
                            
        @if ( count($status)>0)
        <x-laravel-blade-sortable::sortable                 
                group="estado"
                name="{{ $operation_id }}"
                wire:onSortOrderChange="handleSortOrderChange"                
                animation="1000"
                ghost-class="opacity-40"
                drag-handle="drag-handle">
            @foreach ($status as $statu)
            <x-laravel-blade-sortable::sortable-item sort-key="{{ $statu->id }}">

                <div x-data="{mostrar:false}"  class="border border-white rounded-md py-2 px-3 my-2 bg-white shadow-xl relative">                        
                    
                    <div x-data="{openstatus:false}" @click.away="openstatus = false" @close.stop="openstatus = false" class="flex justify-between items-center relative border-b pb-1" style="border-bottom: solid 1px {{ $statu->color }};">
                        <p class="font-bold">{{ $statu->name }}</p>
                        <div @click="openstatus=!openstatus" class="hover:text-green-500 cursor-pointer border rounded-md py-1 px-3 text-sm">
                            <i class="fas fa-ellipsis-h"></i>
                        </div>
                        <div x-show="openstatus" class=" absolute right-1 top-8 mx-2 px-3 py-1 bg-white border rounded-md text-sm">
                            <ol class="gap-4">
                                <li wire:click="editStatus({{ $statu->id }})" @click="mostrar=!mostrar" class="pt-2 hover:bg-slate-200 px-3 cursor-pointer" title="Editar tipo deperación"><i class="fas fa-pencil-alt"></i> Editar</li>
                                <li wire:click="deleteStatus({{ $statu->id }})" class="pt-2 hover:bg-slate-200 px-3 cursor-pointer" title="Eliminar tipo de operacion y estados"><i class="far fa-trash-alt"> </i> Eliminar</li>
                            </ol>
                        </div>
                    </div>
                    

                    @if ($estado)
                        @if ($estado->id == $statu->id)
                        <div class="absolute border-2 border-white bg-gray-200/30 backdrop-blur-md shadow-2xl mx-2 px-3 py-2 w-full rounded-xl -ml-4 mb-4" style="margin-bottom: 10px; z-index: 8;">
                            <form wire:submit.prevent="updateStatus" class="w-full ">
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Nombre estado
                                        </label>
                                        <input wire:model.defer="name" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Nombre de estado">
                                        @error('name')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Código de estado
                                        </label>
                                        <input wire:model.defer="code" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Código de estado">
                                        @error('code')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Orden
                                        </label>
                                        <input wire:model.defer="order" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="number" step="1" min="1" placeholder="Orden de estado">
                                        @error('order')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Mensaje del evento
                                        </label>
                                        <input wire:model.defer="user_note" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Ingrese el mensaje">
                                        @error('user_note')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Color
                                        </label>
                                        <input wire:model.defer="color" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-100 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Color">
                                        @error('color')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 md:mb-0">
                                        <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                            Actualiza kardex?
                                        </label>
                                        <select wire:model.defer="io" class="w-full text-sm rounded-md border-gray-100 active:ring-0 focus:ring-0" name="" id="">
                                            <option class="py-1 px-2" value="-1">sin movimiento</option>
                                            @if ($io)
                                                <option class="py-1 px-2" value="0">Ingreso a kardex <i class="ml-2 fas fa-check-circle text-green-500 font-bold"></i> </option>
                                                <option selected class="py-1 px-2" value="1">(-) Salida d kardex <i class="ml-2 fas fa-minus-circle text-pink-500 font-bold"></i> </option>
                                            @elseif ($io==0)
                                                <option selected class="py-1 px-2" value="0">(+) Ingreso a kardex <i class="ml-2 fas fa-check-circle text-green-500 font-bold"></i> </option>
                                                <option class="py-1 px-2" value="1">Salida de kardex <i class="ml-2 fas fa-minus-circle text-pink-500 font-bold"></i> </option>
                                            @endif
                                            
                                        </select>   
                                        @error('io')
                                            <p class="text-red-500 text-xs italic">{{ $message }}</p>                                            
                                        @enderror
                                    </div>
                                </div>
                                <div class="mb-6">
                                    <div class="w-full px-3 mb-6 flex justify-between ">
                                        <button type="sutmit" class="bg-green-500 hover:bg-green-600 shadow-lg px-3 py-1 rounded-md text-white">Actualizar</button>
                                        <button wire:click="cancelStatus" type="reset" class="bg-pink-500 hover:bg-pink-600 shadow-lg px-3 py-1 rounded-md text-white">Cancelar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif
                    @endif
                    <div class="text-sm w-full">
                        @if ($statu->io ==1)
                            <p class="my-1 py-1 px-2">Salida de deposito 
                                <i class="ml-2 fas fa-minus-circle text-pink-500 font-bold"></i> 
                            </p>
                        @elseif ($statu->io===0)
                            <p class="my-1 py-1 px-2">Ingreso a deposito 
                                <i class="ml-2 fas fa-check-circle text-green-500 font-bold"></i> 
                            </p>
                        @endif
                        <p class="my-1 px-2 text-gray-600">Envio Automatico desactivdo</p>
                        <p class="my-1 px-2 text-gray-600">Código: <span class="px-1 ml-2">{{ $statu->code }}</span>  </p>
                        <p class="my-1 px-2">Color: <span class="px-1 ml-2" style="background: {{ $statu->color }}; "></span></p>
                        <p class="my-1 py-1 px-2 text-xs text-gray-400 ">última Actualización: {{ $statu->updated_at }}</p>
                    </div>
                    
                </div>

            </x-laravel-blade-sortable::sortable-item>
            
            @endforeach
        </x-laravel-blade-sortable::sortable>
        @else
            <div class="px-3 py-4">
                <p class="text-gray-600 font-normal text-sm">No hay lista de Estados</p>
            </div>
        @endif 
        <div class="py-4 px-3 mt-5 w-full">

        </div>
    </div>
    
    @if ( count($status)>0)
    <div class="absolute bottom-0 w-full py-3 px-3 font-bold text-sm bg-gray-200/30 backdrop-blur-sm">
        Total de estados: {{ count($status) }}
    </div>
    @endif

    @if ($emitRoot)
    {{-- actualizamos los estados cuando se movieron de la operación --}}
        <script>
            window.location.reload();
        </script>
    @endif
</div>
