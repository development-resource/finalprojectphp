<div>
    <h1 class="ml-4  text-xl font-semibold">Lista de productos</h1>
                
        <div class="">

            <div class="grid grid-cols-2 sm:grid-cols-3 md:grid-cols-4 p-4 gap-4">
                <div class="w-full">
                    <div class="bg-gradient-to-b from-green-200 to-green-100 border-b-4 border-green-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4 my-auto">
                                <div class="rounded-full p-1 bg-green-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-wallet fa-2x fa-inverse "></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">$3249 <span class="text-green-500"><i class="fas fa-caret-up"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Revenue</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="w-full">
                    <div class="bg-gradient-to-b from-pink-200 to-pink-100 border-b-4 border-pink-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4 my-auto">
                                <div class="rounded-full p-1 bg-pink-600 w-14 h-14  flex justify-center items-center">
                                    <i class="fas fa-users fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center ">
                                <div class="">
                                    <p class="">$3249 <span class="text-pink-500"><i class="fas fa-exchange-alt"></i></span></p>
                                    <p class="uppercase text-gray-600 text-xs">Total Users</p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="w-full">
                    <div class="bg-gradient-to-b from-yellow-200 to-yellow-100 border-b-4 border-yellow-600 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-yellow-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-user-plus fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">2<span class="text-yellow-600"><i class="fas fa-caret-up"></i></span></p>
                                    <h2 class="uppercase text-gray-600 text-xs">New Users</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="w-full">
                    <div class="bg-gradient-to-b from-blue-200 to-blue-100 border-b-4 border-blue-500 rounded-lg shadow-xl p-3">
                        <div class="flex flex-row justify-between content-center my-auto">
                            <div class="flex-shrink pr-4">
                                <div class="rounded-full p-5 bg-blue-600 w-14 h-14 flex justify-center items-center">
                                    <i class="fas fa-server fa-2x fa-inverse"></i>
                                </div>
                            </div>
                            <div class="text-right text-md font-bold flex justify-center items-center">
                                <div class="">
                                    <p class="">152</p>
                                    <h2 class="uppercase text-gray-600 text-xs">Server Uptime</h2>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- lista deproductos --}}
        <div class="w-full my-2">
            <div class="grid grid-cols-1 sm:grid-cols-2 gap-2 my-auto justify-between">
                <div class="my-2 max-w-sm" >   
                    <div class="relative">
                        <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                        </div>
                        <input type="text" wire:model="search" class="block p-4 pl-10 w-full text-sm text-gray-900 bg-white rounded-lg border ring-0 active:ring-1 border-gray-300 focus:ring-blue-500 focus:border-blue-50 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Ingrese un termino" required>
                        <button wire:click.defer="resetsearch" {{ ($search != '')? '':'disabled' }}  class="absolute right-2.5 bottom-2.5 bg-gray-50 {{ ($search == '')? 'text-gray-300':'border text-gray-600 hover:bg-gray-200 cursor-pointer focus:ring-4 focus:outline-none focus:ring-blue-300' }}    font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            <i class="fas fa-eraser"></i>
                        </button>
                    </div>
                </div>
                
                <form wire:submit.prevent="savefile" class="flex justify-center items-center w-full" method="POST">
                    @csrf
                    <label for="dropzone-file" class="flex flex-col justify-center items-center w-full h-auto {{ ($fileupload !=null) ? 'bg-green-200':'bg-gray-50' }} rounded-lg border-2 border-gray-300 border-dashed cursor-pointer dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                        <div class="flex flex-col justify-center items-center py-1 px-3">
                            <svg aria-hidden="true" class="mb-1 w-5 h-5 text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"></path></svg>
                            <p class="mb-2 text-sm text-gray-500 dark:text-gray-400"><span class="font-semibold">Click para adjuntar csv</span> </p>
                            <p class="text-xs text-gray-500 dark:text-gray-400">CSV o TXT (MAX. 3MB)</p>
                            @error('fileupload')
                                <p class="text-pink-500 text-xs font-bold">{{ $message }}</p>
                            @enderror
                        </div>
                        <input wire:model="fileupload" id="dropzone-file" type="file" class="hidden" />
                    </label>
                    <button type="submit" class="text-white text-sm px-4 py-2 rounded-md mx-2 {{ ($fileupload !=null) ? 'bg-green-500 hover:bg-green-600':'hidden' }}">Cargar</button>
                </form> 
            </div>
             

        </div>

        <div class="w-full bg-white rounded-xl shadow-xl">
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 shadow-md border border-white">
                <thead class="text-xs text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400">
                    <tr class="px-2 text-center">
                        <th class="py-4 border-r border-white">Items</th>
                        <th class="py-4 border-r border-white">Código</th>
                        <th class="py-4 border-r border-white hidden sm:table-cell">Nombre</th>
                        <th class="py-4 border-r border-white hidden sm:table-cell ">Detalle</th>
                        <th class="py-4 border-r border-white hidden md:table-cell">Estado</th>
                        <th class="py-4 border-r border-white hidden md:table-cell">Precio</th>
                        <th class="py-4 ">Actions</th>
                    </tr>
                </thead>
                <tbody class="text-gray-600 text-sm font-light text-left">
                    @if ( count($products) > 0 )
                        @foreach ($products as $product)
                        
                            <tr class="border-b border-gray-200 hover:bg-emerald-200">
                                
                                <td class="py-3 px-6 text-left whitespace-nowrap">
                                    <div class="flex items-center">                                        
                                        <span class="font-medium">{{ $product->id }}</span>
                                    </div>
                                </td>
        
                                <td class="py-3 px-6 text-left">
                                    <div class=" ">
                                        <span>{{ $product->code }}</span>
                                    </div>
                                </td>
        
                                <td class="py-3 px-6  hidden sm:table-cell">
                                    <div class="">
                                        {{ $product->name }}
                                    </div>
                                </td>
        
                                <td class="py-3 px-6  hidden sm:table-cell">
                                    <div class="">
                                        {{ $product->description }}
                                    </div>
                                </td>
        
                                <td class="py-3 px-6 hidden md:table-cell">
                                    <div class="py-1 px-3 ">
                                        {!! $product->status ? '<i class="fas fa-check-circle text-green-500"></i>' : '<i class="fas fa-times-circle text-pink-500"></i>' !!}
                                    </div>
                                </td>
        
                                <td class="py-3 px-6 text-center hidden md:table-cell">
                                    <div class="text-center">
                                        {{ $product->price }}
                                    </div>
                                </td>
        
                                <td class="py-3 px-6 text-center ">
                                    <div class="flex item-center justify-center realtive" x-data="{abrir: false}">
                                        <div  class="flex gap-1">
                                            
                                            <a href="{{ route('kardexs', $product->id) }}" class="bg-white cursor-pointer text-gray-700 hover:text-teal-600 border px-2 py-1 hover:shadow-lg rounded-md " title="Kardex del producto"> 
                                                <i class="fas fa-cubes"></i>
                                            </a>
                                            
                                        </div>   
                                        
                                        
                                    </div>
                                </td>                            
                            </tr>
                            
                        
                        @endforeach


                    @endif
                    
        
                </tbody>

                @if ( count($products) > 0 )

                <tfoot class="">
                    <tr>
                        <td colspan="7">
                            <div class="text-gray-700 uppercase bg-gray-100 dark:bg-gray-700 dark:text-gray-400 py-3  px-3 text-md">
                                
                                {{ $products->links('paginate-livewire') }}

                            </div>
                        </td>
                    </tr>
                </tfoot>
                @endif

            </table>
        </div>
</div>
