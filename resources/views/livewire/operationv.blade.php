<div class="w-full relative">
    <div class="w-full rounded-md bg-white py-2 px-1 sm:px-3 sm:flex  gap-2" style="z-index: 10;">
        <div class="flex gap-2 items-center my-auto">
            <p class="text-gray-400"><i class="fas fa-braille"></i></p>
            <p class="">Corporation Dev <i class="fas fa-star text-gray-400"></i> <span class="text-xs text-gray-300">4.8 view</span></p>
            
        </div>
        <div class="flex items-center my-auto gap-2 relative" x-data="{open:false}" @click.away="open = false" @close.stop="open = false">
            <button class=" border bg-green-500 text-white hover:bg-green-600 px-2 py-1 rounded-md" @click="open=!open">
                <i class="fas fa-plus"></i> Nuevo servicio
            </button>
            <div x-show="open" class="absolute right-4 top-10 mx-2 px-3 py-1 bg-white border rounded-md text-md w-80 bg-gray-200/30 backdrop-blur-md shadow-xl" style="z-index: 11;">
                @if ($operation == null)
                <form wire:submit.prevent="saveOperation" class="w-full mt-3" method="POST">
                    @csrf
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Nombre Operación
                            </label>
                            <input  wire:model.defer="name" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Nombre de la operación">
                            @error('name')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 md:mb-0">
                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                Código ope
                            </label>
                            <input wire:model.defer="code" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Código de la operación">
                            @error('code')
                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <div class="w-full px-3 mb-6 flex justify-between ">
                            <button type="submit" class="bg-green-500 hover:bg-green-600 shadow-lg px-3 py-1 rounded-md text-white">Agregar</button>
                            <button wire:click="cancelOperation" @click="open=false" class="bg-pink-500 hover:bg-pink-600 shadow-lg px-3 py-1 rounded-md text-white">Cancelar</button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
    <div class="mt-2 py-0 -mr-4  w-full relative " > 
        <div class="overflow-auto flex  whitespace-nowrap">
            @if ( count($operations)>0)
                @foreach ($operations as $operation)
                
                <div class="mr-3 w-96 rounded-md bg-white relative">
                    <div x-data="{open:false}" class="">
                        <div x-data="{openm:false}" @click.away="openm = false" @close.stop="openm = false" class=" flex justify-between py-2 px-5 items-center border-b border-gray-200" style="z-index: 10;">
                            <div class="font-bold py-2">{{ $operation->name }}</div>
                            <div @click="openm=!openm" class="hover:text-green-500 cursor-pointer border rounded-md py-1 px-3 ">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div x-show="openm" class="absolute right-4 top-12 mx-2 px-3 py-1 bg-white border rounded-md text-sm" style="z-index: 10;">
                                <ol class="gap-4">
                                    <li @click="openm=false" wire:click.prevent="FindOperation({{ $operation->id }})"  class="pt-2 hover:bg-slate-200 px-3 cursor-pointer" title="Editar tipo deperación"><i class="fas fa-pencil-alt"></i> Editar</li>
                                    <li @click="openm=false" wire:click.prevent="$emitTo('statusv', 'addStatus',{{ $operation->id }})"  class="pt-2 hover:bg-slate-200 px-3 cursor-pointer" title="Editar tipo deperación"><i class="fas fa-plus"></i> Agregar</li>
                                    <li @click="openm=false" wire:click.prevent="deleteConfirm({{ $operation->id }})" class="pt-2 hover:bg-slate-200 px-3 cursor-pointer" title="Eliminar tipo de operacion y estados"><i class="far fa-trash-alt"> </i> Eliminar</li>
                                </ol>
                            </div>
                        </div>
                        @if ($operation->id == $operation_edit_id)
                            <div class="pt-3 absolute border-2 -mt-4 border-white bg-gray-200/30 backdrop-blur-sm shadow-xl mx-2 px-3 py-2 w-11/12 rounded-xl mr-4 " style="z-index: 10;">
                                <form wire:submit.prevent="updateOperation" class="w-full" method="POST">
                                    @csrf
                                    @method('update')
                                    <div class="mb-6">
                                        <div class="w-full px-3 mb-6 md:mb-0">
                                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                                Nombre Operación
                                            </label>
                                            <input wire:model.defer="name" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Nombre de la operación">
                                            @error('name')
                                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-6">
                                        <div class="w-full px-3 mb-6 md:mb-0">
                                            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" >
                                                Código ope
                                            </label>
                                            <input wire:model.defer="code" class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" type="text" placeholder="Código de la operación">
                                            @error('code')
                                                <p class="text-red-500 text-xs italic">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-6">
                                        <div class="w-full px-3 mb-6 flex justify-between ">
                                            <button type="submit" class="bg-green-500 hover:bg-green-600 shadow-lg px-3 py-1 rounded-md text-white">Actualizar</button>
                                            <button wire:click="cancelOperation" @click="open=false" class="bg-pink-500 hover:bg-pink-600 shadow-lg px-3 py-1 rounded-md text-white">Cancelar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                    
                    <div class="">
                        <livewire:statusv :operation_id="$operation->id" :wire:key="$operation->id" />
                        
                    </div>
                </div>

                @endforeach
            @else
                <div class="px-4 text-gray-600 py-3">
                    No hay lista de servicios
                </div>
            @endif
        </div>
    </div>

    <x-jet-confirmation-modal wire:model="confirmingDeletion">
        <x-slot name="title">
            
            <strong class="font-bold text-pink-600"> ¿Esta seguro de eliminar?</strong>
        </x-slot>
    
        <x-slot name="content">
            <strong class="font-bold text-orange-600"> Nota:</strong>
            <p class="">
                Se eliminará la operación y la lista de estados; no podran ser recuperados.
            </p>
        </x-slot>
    
        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$toggle('confirmingDeletion')" wire:loading.attr="disabled">
                Anular
            </x-jet-secondary-button>
    
            <x-jet-danger-button class="ml-2" wire:click="deleteOperation" wire:loading.attr="disabled">
                Quiero eliminar
            </x-jet-danger-button>
        </x-slot>
    </x-jet-confirmation-modal>

</div>
