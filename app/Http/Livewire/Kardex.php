<?php

namespace App\Http\Livewire;

use App\Models\Karde;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Kardex extends Component
{
    public $kardexes = [], $product_id, $product , $cantystatus = [], $tomonth = '';

    public function mount($product_id)
    {
        $this->tomonth  = Carbon::now()->format('Y-m').'-01 00:00';
        $this->product = Product::where('id','=',$product_id)->first();

        $this->kardexes = Karde::where('product_id','=', $product_id)->get();
        $this->product_id = $product_id;
        $this->cantystatus = DB::table('orderdetails as od')
        ->selectRaw('sum(od.canty) as canty, oh.status as statu, s.name as name, s.color as color')
        ->join('orderheaders as oh','od.orderheader_id', '=', 'oh.id')
        ->join('products as p','od.product_id', '=', 'p.id')
        ->join('status as s', 'oh.status', '=', 's.id')
        ->where('p.id', '=',$this->product_id)
        ->Where('oh.created_at', '>=',$this->tomonth)
        ->groupBy('oh.status','s.name','s.color')
        ->orderBy('oh.status','asc')
        ->get();

    }

    public function render()
    {

        return view('livewire.kardex');

    }
}
