<?php

namespace App\Http\Livewire;

use App\Models\Role;
use Livewire\Component;

class Rolesuser extends Component
{
    public $roles=[];

    public function mount()
    {
        $this->roles = Role::OrderBy('id','asc')->get();
    }

    public function render()
    {
        return view('livewire.rolesuser');
    }
}
