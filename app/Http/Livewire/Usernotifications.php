<?php

namespace App\Http\Livewire;

use App\Models\Usernotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Usernotifications extends Component
{
    public $notifications = [], $today;
    
    protected $listeners = ['mount'];

    public function mount()
    {
        $this->today = Carbon::now()->format('Y-m-d');

        $this->notifications = Auth::user()->notifications;
    }
    public function render()
    {
        return view('livewire.usernotifications');
    }

    public function readmessage(Usernotification $usernotification_id)
    {
        $noti = $usernotification_id->update([
            "status"=>1,
        ]);
        //return dd($usernotification_id);
        // return dd($noti);
        $this->notifications = Auth::user()->notifications;

        return $this->emit('mount');
    }
}
