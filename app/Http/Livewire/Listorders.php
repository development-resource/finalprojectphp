<?php

namespace App\Http\Livewire;

use App\Models\Orderdetail;
use App\Models\Orderheader;
use App\Models\Statu;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Spatie\QueryBuilder\QueryBuilder;
class Listorders extends Component
{

    public $orders = [], $order, $headers, $query, $searchtext, $details=[];

    public $search, $limit=10, $base=0,$estados, $statu_id;

    protected $listeners = ['render'];

    public function mount()
    {
        $this->estados= Statu::all();
       
        $modelo = new Orderheader();

        $query = $modelo->newQuery();
        
        //$query =Orderheader::where('id','=',1)->get();

        //return dd($res->estado());

        $this->orders = $query->with('orderdetails','estado');

        if($this->search)
        {
            $this->orders =$query->orWhere('num_fact','like', '%'.$this->search.'%')
            ->orWhere('delivery_day','like', '%'.$this->search.'%');
        }
        
        if($this->statu_id)
        {
            $this->orders =$query->where('status','=', $this->statu_id);
        }

        if($this->limit)
        {
            $this->orders = $query->skip($this->base)->take($this->limit)->get();
        }

        // return dd($this->orders->estado);
    }

    public function limitrows( int $rows)
    {
        if($rows== 1)
        {
            $this->limit = -1; 
        }

        $this->limit = (int) $rows;

        $this->columsfilter();
    }

    public function render()
    {
        // return dd($this->notifications);

        return view('livewire.listorders');
    }

    public function filterStatu($status_id)
    {
        $this->statu_id = $status_id;

        $this->columsfilter();

    }

    public function searchtext()
    {
        if($this->search == "")
        {
            $this->search = null;
        }
        $this->columsfilter();
    }


    public function statusall()
    {
        $this->statu_id = null;

        $this->columsfilter();
    }

    public function columsfilter()
    {
        $modelo = new Orderheader();

        $query = $modelo->newQuery();
        
        $this->orders = $query->with('orderdetails','estado');

        if($this->search)
        {
            $this->orders = $this->orders->orWhere('num_fact','like', '%'.$this->search.'%')
            ->orWhere('delivery_day','like', '%'.$this->search.'%');
        }

        if($this->statu_id)
        {
            $this->orders =$this->orders->where('status','=', $this->statu_id);
        }

        if($this->limit)
        {
            $this->orders = $this->orders->skip($this->base)->take($this->limit)->get();
        }

        return $this->emit('render');
    }

    public function listdetail($order_id)
    {
        $this->details = [];
        $this->details = Orderdetail::with('product')->where('orderheader_id','=',$order_id)->orderBy('id')->get();
        //return dd($details);

        
    }


}
