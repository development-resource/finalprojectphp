<?php

namespace App\Http\Livewire;

use App\Models\Operation;
use App\Models\Statu;
use Livewire\Component;

class Operationv extends Component
{
    public $operations = [], $operation_edit_id, $operation, $confirmingDeletion=false ;
    
    public $name, $code, $operation_delete_id;
    
    protected $listeners = ['upload','render'];

    public function mount()
    {
        $this->operations = Operation::orderBy('id','desc')->get();
    }

    
    public function render()
    {
        return view('livewire.operationv');
    }

    public function FindOperation($operation_id)
    {
        $ope = Operation::find($operation_id);
        $this->operation_edit_id = $operation_id;
        $this->operation = $ope;
        $this->name = $ope->name; 
        $this->code = $ope->code;
    }

    public function saveOperation()
    {

        $this->validate([
            "name"=>"required|string|max:50|min:4|unique:operations",
            "code"=>"required"
        ]);

        Operation::create([
            "name"=> $this->name,
            "code"=> $this->code
        ]);

        $this->name=null;
        $this->code=null;
        
        $this->upload();

    }

    public function updateOperation()
    {

        $this->validate([
            "name"=>"required|string|max:50|min:4|unique:operations",
            "code"=>"required"
        ]);

        
        if($this->operation!=null)
        {
            $this->operation->update([
                "name"=> $this->name,
                "code"=> $this->code
            ]);
        }

        $this->operation_edit_id = null;
        $this->operation = null;
        $this->name = null;
        $this->code = null;

        $this->upload();

    }

    public function deleteOperation()
    {
        $this->operation_delete_id->delete();

        $this->operation_delete_id = null;
        $this->confirmingDeletion = false;
        $this->upload();
    }

    public function upload()
    {
        
        $this->operations = Operation::orderBy('id','desc')->get();
        return $this->emit('render');
    }

    public function cancelOperation()
    {
        $this->operation_edit_id = null;
        $this->operation = null;
        $this->name = null;
        $this->code = null;
    }

    public function deleteConfirm(Operation $operation_id)
    {

        $this->operation_delete_id = $operation_id;

        $this->confirmingDeletion = true;
        
    }

    

}
