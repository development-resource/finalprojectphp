<?php

namespace App\Http\Livewire;

use App\Models\Product;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Str;

class Productv extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $search = '', $fileupload;
    
    protected $listeners = ['render','addStatus'];

    public function mount()
    {
        // $this->status = Product::where('operation_id','=',$operation_id)->orderBy('order','asc')->get();
    }

    public function resetsearch()
    {
        $this->search = '';
    }
 
    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function query()
    {
        return Product::where('name','like','%'.$this->search.'%')
        ->orWhere('code','like','%'.$this->search.'%')
        ->orWhere('sku','like','%'.$this->search.'%')
        ->orderBy('stock','desc')
        ->orderBy('status','desc')
        ->paginate(10);
    }

    public function render()
    {
        return view('livewire.productv',[
            'products' => $this->query()
        ] );

    }

    public function searchCustom()
    {
        
    }

    public function savefile()
    {

        $this->validate([
            'fileupload' => 'required|file|mimes:txt,csv|max:10',
        ]);

        $path = $this->fileupload->getRealPath();

        $data = array_map('str_getcsv', file($path));

        $this->data = $data;

        if(empty($data)){return;}

        if (count($data) > 0) 
        {
            $this->csv_data = array_slice($data, 1, count($data), false); 

            foreach($this->csv_data as $filas)
            {
                foreach($filas as $columns) 
                {
                    $fields = explode(";", $columns);

                    $sku= (trim($fields[0]) != null) ? trim($fields[0]) : null;
                    $code = (trim($fields[1]) != null) ? trim($fields[1]) : "";
                    $name = (trim($fields[2]) != null) ? trim($fields[2]) : "";
                    $description = (trim($fields[3]) != null) ? trim($fields[3]) : null;
                    $slug = Str::slug($name);
                    $price = (trim($fields[4]) != null) ? trim($fields[4]) : null;
                    $pricebase = (trim($fields[5]) != null) ? trim($fields[5]) : "";
                    $isc = (trim($fields[6]) != null) ? trim($fields[6]) : null;
                    $igv = (trim($fields[7]) != null) ? trim($fields[7]) : false;
                    $stock = (trim($fields[8]) != null) ? trim($fields[8]) : false;
                    $status = (trim($fields[9]) != null) ? trim($fields[9]) : false;
                    $path_image = null;
                    $branchoffice_id = (trim($fields[10]) != null) ? trim($fields[10]) : 1;

                    $igv = filter_var($igv, FILTER_VALIDATE_BOOLEAN);
                    $status = filter_var($status, FILTER_VALIDATE_BOOLEAN);
                    $stock = filter_var($stock, FILTER_VALIDATE_BOOLEAN);

                    if($code!="" && $name!="")
                    {
                        $newprod = Product::create([
                            'sku'=>$sku,
                            'code'=>$code,
                            'name'=>$name,
                            'description'=>$description,
                            'slug'=>$slug,
                            'price'=>$price,
                            'pricebase'=>$pricebase,
                            'isc'=>$isc,
                            'igv'=>$igv,
                            'stock'=>$stock,
                            'status'=>$status,
                            'path_image'=>$path_image,
                            'branchoffice_id'=>$branchoffice_id,
                        ]);
                    }

                }
            }
        }

        $this->fileupload = null;
        return $this->emit('render');

    }


}
