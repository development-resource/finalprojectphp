<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Componentes extends Component
{

    public $modul_id;

    public function mount()
    {
        $this->modul_id = 1;
    }

    public function render()
    {
        return view('livewire.componentes');
    }

    public function moduloId($id)
    {
        $this->modul_id = $id;
    }

}
