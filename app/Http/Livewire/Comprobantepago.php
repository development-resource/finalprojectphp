<?php

namespace App\Http\Livewire;

use App\Models\Vouchertype;
use Livewire\Component;

class Comprobantepago extends Component
{
    public $pagos=[];

    public function mount()
    {
        $this->pagos = Vouchertype::OrderBy('id','desc')->get();
    }

    public function render()
    {
        return view('livewire.comprobantepago');
    }



}
