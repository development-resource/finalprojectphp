<?php

namespace App\Http\Livewire;

use App\Models\Orderheader;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Filters\SelectFilter;

class OrderheaderTable extends DataTableComponent
{
    // protected $model = Orderheader::class;

    public $columnSearch = [
        'num_fact' => null,
        'delivery_day' => null,
    ];

    public array $bulkActions = [
        'exportSelected' => 'Export',
    ];
    
    public function bulkActions(): array
    {
        return [
            'exportSelected' => 'Export',
        ];
    }

    public function exportSelected()
    {
        foreach($this->getSelected() as $item)
        {
            return dd($this->getSelected() );
        }
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setPerPageAccepted([10, 25, 50, 100]);
        $this->setPaginationVisibilityStatus(true);
        $this->setFilterPillsStatus(true);
        // $this->setFilterLayoutSlideDown(); filtro salta a otra linea
        $this->setBulkActionsStatus(true);
        $this->setBulkActionsEnabled();
        $this->setBulkActions([
            'exportSelected' => 'Export',
        ]);

    }

    public function builder(): Builder
    {
        return Orderheader::query()
            ->with('orderdetails') // Eager load anything
            //->join() // Join some tables
            ->select(); // Select some things
    }

    public function filters(): array
    {
        return [
            SelectFilter::make('status')
            ->options([
                '' => 'All',
                '1' => 'Yes',
                '0' => 'No',
            ])
            ->filter(function(Builder $builder, string $value) {
                if ($value === '1') {
                    $builder->where('status', true);
                } elseif ($value === '0') {
                    $builder->where('status', false);
                }
            }),
        ];
    }

   
    // relaciones con otras tablas
    // https://rappasoft.com/docs/laravel-livewire-tables/v2/usage/the-query
    public function columns(): array
    {
        return [
            Column::make("Id", "id")
                ->sortable()->collapseOnMobile(),

            Column::make("#Factura", "num_fact")
                ->sortable()->searchable(),

            Column::make("Fecha Entrega", "delivery_day")
                ->sortable()->searchable(),

            Column::make("Estado", "status")
                ->sortable()->searchable()->collapseOnMobile(),

            //Column::make("Estados", "orderstatus.user_id")->sortable()->searchable()->collapseOnMobile(),
            Column::make("Estados", "estado.name"),

            Column::make("Created at", "created_at")
                ->sortable()->collapseOnMobile(),
                
            Column::make("Updated at", "updated_at")
                ->sortable()->collapseOnTablet(),
        ];
    }
}
