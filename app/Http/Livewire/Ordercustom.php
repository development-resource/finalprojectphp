<?php

namespace App\Http\Livewire;

use App\Models\Orderdetail;
use App\Models\Orderheader;
use App\Models\Orderstatu;
use Livewire\Component;

class Ordercustom extends Component
{
    public $order_id, $order, $orderdetails= [], $orderstatus=[];

    public function mount($order_id)
    {
        $this->order_id = $order_id;

        $this->order = Orderheader::find($order_id);

        $this->orderstatus = Orderstatu::with('user')->where('orderheader_id','=',$order_id)->orderBy('created_at','desc')->get();

        $this->orderdetails = Orderdetail::where('orderheader_id','=',$order_id)->get();
    }

    public function render()
    {
        return view('livewire.ordercustom');
    }
}
