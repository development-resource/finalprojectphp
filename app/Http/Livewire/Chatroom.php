<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Chatroom extends Component
{
    public function render()
    {
        return view('livewire.chatroom');
    }
}
