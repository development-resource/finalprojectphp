<?php

namespace App\Http\Livewire;

use App\Models\Statu;
use Livewire\Component;

class Statusv extends Component
{
    public $operation_id, $status = [], $estado, $statu_edit_id, $addStatus_id;
    public $name, $code, $color, $order, $io, $user_note;
    public $statusdragable=[], $statu=[],$__v_raw, $emitRoot=false;

    protected $listeners = ['render','addStatus'];

    public function mount($operation_id)
    {
        $this->status = Statu::where('operation_id','=',$operation_id)->orderBy('order','asc')->get();
    }

    public function render()
    {
        return view('livewire.statusv');
    }

    public function editStatus($status_id)
    {
        $est = Statu::find($status_id);
        
        $this->estado =$est;
        $this->statu_edit_id =$status_id;
        $this->name =$est->name;
        $this->code =$est->code;
        $this->order =$est->order;
        $this->color = $est->color;
        $this->io =$est->io;
        $this->user_note =$est->user_note;

    }

    public function updateStatus()
    {
        $this->validate([
            "name"=>"required|max:50|min:5",
            "code"=> "required",
            "order"=>"required|numeric",
            "operation_id"=>"required|numeric",
        ]);

        $this->estado->update([
            "name"=> $this->name,
            "code"=> $this->code,
            "order"=> $this->order,
            "color" => $this->color,
            "user_note" => ($this->user_note!=null) ? $this->user_note: null,
            "io"=>($this->io !== -1) ? $this->io: null,
            "operation_id"=> $this->operation_id,
        ]);
        
        $this->cancelStatus();

        return $this->emit('render');
    }

    public function deleteStatus(Statu $status_id)
    {
        $status_id->delete();
        return $this->emit('render');
    }


    public function cancelStatus()
    {
        $this->estado    = null;
        $this->statu_edit_id    = null;
        $this->name     = null;
        $this->code     = null;
        $this->order    = null;
        $this->color    = null;
        $this->io       = null;
        $this->user_note  = null;
    }

    public function upload()
    {
        $this->status = Statu::where('operation_id','=',$this->operation_id)->orderBy('order','asc')->get();
        return $this->emit('render');
    }

    public function addStatus($operation_id)
    {
        $this->addStatus_id=$operation_id;
        $this->cancelStatus();
        return $this->emit('render');
    }

    public function newStatu()
    {
        $this->validate([
            "name"=>"required|max:50|min:5",
            "code"=> "required",
            "order"=>"required|numeric",
            "operation_id"=>"required|numeric",
        ]);

        Statu::create([
            "name"=> $this->name,
            "code"=> $this->code,
            "order"=> $this->order,
            "color" => $this->color,
            "user_note" => ($this->user_note!=null) ? $this->user_note: null,
            "io"=>($this->io !== -1) ? $this->io: null,
            "operation_id"=> $this->operation_id,
        ]);
        
        $this->cancelStatus();
        $this->addStatus_id=null;
        $this->upload();
        return $this->emit('render');

    }

    public function canceAdd()
    {
        $this->addStatus_id=null;
        $this->cancelStatus();
        return $this->emit('render');
    }

    public function handleSortOrderChange($sortOrder, $previousSortOrder, $name, $from, $to)
    {

        $dif_id = array_diff($previousSortOrder,$sortOrder);
        foreach($sortOrder as $order=>$id)
        {
            Statu::where('id',$id)->update(["order"=>$order]);
        }

        if($dif_id)
        {
            Statu::where('id',$dif_id)->update(["operation_id"=>$to[0]]);
            $this->emitRoot = true;
        }
        
        $this->upload();
    }


}
