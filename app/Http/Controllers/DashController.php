<?php

namespace App\Http\Controllers;

use App\Models\Orderheader;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{
    public function index()
    {
        $productos = Product::paginate(12);

        $orderheaders = Orderheader::paginate(10); //paginate   simplePaginate

        return view('dashboard', compact('productos','orderheaders',));
    }

    
    public function orderAdmin($order_id)
    {
        return view('kardex.order', compact('order_id'));
    }

    public function message()
    {
        return view('kardex.chatfriend');
    }

    public function user()
    {
        return view('kardex.users');
    }

    public function components()
    {
        return view('kardex.componente');
    }

    public function grafics()
    {
        return view('kardex.graficas');
    }
    
    public function servicios()
    {
        return view('kardex.typeservices');
    }
}
