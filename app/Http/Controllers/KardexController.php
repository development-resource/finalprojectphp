<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KardexController extends Controller
{
    public function index($product_id)
    {
        return view('kardex.index' , compact('product_id'));
    }
}
