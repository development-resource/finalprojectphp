<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function kardes()
    {
        return $this->belongsTo(Karde::class);
    }

    

}
