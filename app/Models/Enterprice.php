<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Enterprice extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function brachoffice()
    {
        return $this->hasMany(Branchoffice::class,'enterprice_id');
    }
    
}
