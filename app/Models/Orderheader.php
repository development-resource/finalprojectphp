<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderheader extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function orderstatus()
    {
        return $this->hasMany(Orderstatu::class,'orderheader_id');//->withPivot('id')->orderBy('id','desc')->first();
    }

    public function orderdetails()
    {
        return $this->hasMany(Orderdetail::class,'orderheader_id');
    }

    public function estado()
    {
       return $this->belongsTo(Statu::class,'status','id');
    }





}
