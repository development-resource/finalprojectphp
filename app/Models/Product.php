<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function orderdetails()
    {
        return $this->hasMany(Orderdetail::class,'product_id');
    }

    public function branchoffice()
    {
        return $this->belongsTo(Branchoffice::class,'branchoffice_id');
    }

    public function balance()
    {
        return $this->hasMany(balance::class,'product_id');
    }

    public function kardes()
    {
        return $this->hasMany(Karde::class,'product_id');
    }
    
}
