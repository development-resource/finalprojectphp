<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function  orderheader()
    {
        return $this->belongsTo(Orderheader::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    

}
