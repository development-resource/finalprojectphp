<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branchoffice extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function products()
    {
        return $this->hasMany(Product::class,'branchoffice_id');
    }

    public function enterprice()
    {
        return $this->belongsTo(Enterprice::class,'enterprice_id');
    }

    
}
