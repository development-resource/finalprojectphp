<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statu extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function orderstatus()
    {
        return $this->hasMany(Orderstatu::class);
    }

    public function orderheader()
    {
        return $this->hasMany(Orderheader::class);
    }
    
}
