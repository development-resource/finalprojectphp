<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orderstatu extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    

    public function status()
    {
        return $this->belongsTo(status::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderheader()
    {
        return $this->belongsTo(Orderheader::class);
    }

}
